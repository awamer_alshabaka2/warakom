import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:waraqukum/core/utils/cache_helper.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/core/utils/statics.dart';
import 'package:waraqukum/domain/models/user_model.dart';

class Utils {
  static Future? openScreen(BuildContext? context, Widget screen,
      {bool replacment = false, bool remove = false}) async {
    if (context == null) {
      return null;
    }
    if (remove) {
      return Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => screen),
        (route) => false,
      );
    } else if (replacment) {
      return Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => screen));
    } else {
      return Navigator.push(
          context, MaterialPageRoute(builder: (context) => screen));
    }
  }

  static void showMsg(String msg, {bool error = false}) {
    Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 1,
      backgroundColor: error ? Colors.red : primaryColor,
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }

  static Future<ImageSource?> showImageSource(BuildContext context) async {
    if (Platform.isIOS) {
      return showCupertinoModalPopup<ImageSource>(
          context: context,
          builder: (context) => CupertinoActionSheet(
                actions: [
                  CupertinoActionSheetAction(
                    child: const Text('Camera'),
                    onPressed: () =>
                        Navigator.of(context).pop(ImageSource.camera),
                  ), // CupertinoActionSheetAction
                  CupertinoActionSheetAction(
                    child: const Text('Gallery'),
                    onPressed: () =>
                        Navigator.of(context).pop(ImageSource.gallery),
                  ), // CupertinoActionSheetAction
                ],
              ));
    } else {
      return showModalBottomSheet(
          context: context,
          builder: (context) =>
              Column(mainAxisSize: MainAxisSize.min, children: [
                ListTile(
                  leading: const Icon(Icons.camera_alt),
                  title: const Text('Camera'),
                  onTap: () => Navigator.of(context).pop(ImageSource.camera),
                ), // ListTile
                ListTile(
                  leading: const Icon(Icons.image),
                  title: const Text('Gallery'),
                  onTap: () => Navigator.of(context).pop(ImageSource.gallery),
                ) // ListTile
              ]));
    }
  }

  static UserModel? getUserModel() {
    try {
      final jsonUser = jsonDecode(CacheHelper.getData(key: Statics.userModel));
      UserModel user = UserModel.fromJson(jsonUser);
      return user;
    } catch (e) {
      debugPrint(e.toString());
      return null;
    }
  }

  static Future<File?> pickImage(ImageSource source) async {
    try {
      final image = await ImagePicker().pickImage(source: source);
      if (image == null) return null;
      final imageTemporary = File(image.path);
      return imageTemporary;
    } catch (e) {
      debugPrint('Failed to pick image: $e');
      return null;
    }
  }
}
