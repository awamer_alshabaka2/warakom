import 'package:flutter/material.dart';

const Color primaryColor = Color(0xff202020);
const Color secondaryColor = Color(0xff3D9B72);
const Color quartzColor = Color(0xff505050);
const Color middleGray = Color(0xff777777);
const Color blue = Color(0xff2885BF);
const Color darkGray = Color(0xff292D32);
const Color dimGray = Color(0xff707070);
const Color offWhite = Color(0xffF5F5F5);
// const Color mauveColorDark = Color(0xff2A4270);
// const Color mauveColorlight = Color(0xff355587);
// const Color orange = Color(0xffFC8E00);
// const Color borderMainColor = Color(0xffFDF5E8);
// const List<Color> gradientButton = [mauveColorlight, mauveColorDark];
