import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:waraqukum/screens/widgets/button_widget.dart';
import 'package:waraqukum/screens/widgets/resource_item_photo.dart';
import 'package:waraqukum/screens/widgets/resource_section2.dart';
import 'package:waraqukum/screens/widgets/shimmer_loading_skeleton.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class ResourceWidget extends StatelessWidget {
  const ResourceWidget({
    super.key,
    required this.image,
    this.height,
    this.isHomeScren = false,
    required this.index,
  });

  final String image;
  final double? height;
  final bool isHomeScren;
  final String index;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        isHomeScren
            ? const SizedBox()
            : TeacherNameWidget(image: image, index: index),
        Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          ResourceItemPhoto(image: image),
          Container(
            decoration: BoxDecoration(
              border: Border.symmetric(
                vertical: BorderSide(
                  color: const Color(0xFFF3F3F3).withOpacity(0.97),
                ),
              ),
            ),
            child: const ResourceSection2(),
          ),
          isHomeScren
              ? TeacherNameWidget(image: image, index: index)
              : ButtonWidget(
                  height: 40,
                  borderRadius:
                      const BorderRadius.vertical(bottom: Radius.circular(7)),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const TextWidget(
                          title: "أضف للسلة",
                          color: Colors.white,
                          fontSize: 12,
                        ),
                        const SizedBox(width: 8),
                        Image.asset(
                          "assets/images/shopping-cart.png",
                          width: 14,
                          height: 14,
                          fit: BoxFit.fill,
                        )
                      ]),
                )
        ])
      ],
    );
  }
}

class TeacherNameWidget extends StatelessWidget {
  TeacherNameWidget({
    super.key,
    required this.image,
    required this.index,
  });

  final String image;
  final String index;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 12),
      child: Row(
        children: [
          Hero(
            tag: index,
            child: CachedNetworkImage(
              imageUrl: image,
              imageBuilder: (context, imageProvider) => CircleAvatar(
                backgroundImage: CachedNetworkImageProvider(image),
                radius: 20,
              ),
              placeholder: (context, url) => const ShimmerLoading(
                width: 30,
                height: 30,
                shape: BoxShape.circle,
              ),
              errorWidget: (context, url, error) => const ShimmerLoading(
                width: 30,
                height: 30,
                shape: BoxShape.circle,
              ),
            ),
          ),
          const SizedBox(width: 10),
          const TextWidget(
            title: "منى المنشاوي",
            color: Colors.black,
          ),
        ],
      ),
    );
  }
}
