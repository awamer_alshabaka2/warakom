import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class ButtonWidget extends StatelessWidget {
  final String title;
  final String? fontFamily;
  final double width, height, radius;
  final Widget? child;
  final Gradient? gradient;
  final double? fontSize;
  final FontWeight? fontweight;
  final Alignment? alignment;
  final Color? textColor, buttonColor, borderColor;
  final void Function()? onTap;
  final double? borderWidth;
  final BorderRadiusGeometry? borderRadius;

  const ButtonWidget(
      {Key? key,
      this.gradient,
      this.title = "OK",
      this.width = 314.0,
      this.height = 56.0,
      this.onTap,
      this.fontFamily,
      this.child,
      this.fontSize,
      this.fontweight,
      this.alignment,
      this.textColor = Colors.white,
      this.buttonColor = secondaryColor,
      this.borderColor,
      this.radius = 8.0,
      this.borderWidth,
      this.borderRadius})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        gradient: gradient,
        borderRadius: BorderRadius.circular(radius),
      ),
      child: ElevatedButton(
        onPressed: onTap,
        style: ButtonStyle(
            backgroundColor:
                MaterialStateProperty.all<Color>(buttonColor ?? primaryColor),
            overlayColor: MaterialStateProperty.all<Color>(Colors.transparent),
            shadowColor: MaterialStateProperty.all<Color>(Colors.white),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: borderRadius ?? BorderRadius.circular(radius),
                side: BorderSide(
                  color: borderColor ?? Colors.transparent,
                  width: borderWidth ?? 1.0,
                ),
              ),
            )),
        child: Align(
          alignment: alignment ?? Alignment.center,
          child: child ??
              TextWidget(
                fontWeight: fontweight ?? FontWeight.w500,
                title: title,
                fontSize: fontSize ?? 18,
                // fontFamily: fontFamily ?? "poppins",
                color: textColor ?? primaryColor,
              ),
        ),
      ),
    );
  }
}

class TextButtonWidget extends StatelessWidget {
  const TextButtonWidget({
    super.key,
    required this.function,
    required this.text,
    this.fontweight,
    this.color,
    this.size,
    this.fontFamily,
  });
  final Function function;
  final String text;
  final double? size;
  final Color? color;
  final FontWeight? fontweight;
  final String? fontFamily;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {
        function();
      },
      child: TextWidget(
        title: text,
        fontWeight: fontweight ?? FontWeight.w500,
        // fontFamily: fontFamily ?? "poppins",
        fontSize: size ?? 16,
        color: color ?? primaryColor,
        //  style: TextStyle(color: AppColors.secondary),
      ),
      // style: TextButton.styleFrom(
      //   elevation: 0,
      //   textStyle: TextStyle(
      //     fontWeight: FontWeight.w600,
      //     fontSize: (width <= 550) ? 13 : 17,
      //   ),
      // ),
    );
  }
}
