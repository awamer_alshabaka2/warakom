import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class TextFieldWidget extends StatelessWidget {
  final String? hintText, label, errorText;
  final TextInputType type;
  final bool password;
  final bool expanded;
  final Color activeBorderColor, borderColor, backgroundColor;
  final bool floatingHint;
  final int? maxLines;
  final int? minLines;
  final void Function()? onTap;
  final TextAlign textalign;
  final int? maxLengh;
  final TextDirection? textdirection;
  final EdgeInsetsDirectional? contentPadding;
  final double borderRadius;
  final Widget? prefixIcon, suffixIcon, suffixWidget, prefixWidget;
  final TextEditingController? controller;
  final InputDecoration? inputDecoration;
  final ValueChanged<String>? onChanged;
  final ValueChanged<String?>? onSaved;
  final String? Function(String?)? validator;

  const TextFieldWidget(
      {this.onChanged,
      this.onSaved,
      this.validator,
      this.onTap,
      this.password = false,
      this.expanded = false,
      this.floatingHint = true,
      this.type = TextInputType.text,
      this.hintText = "",
      this.label,
      this.textalign = TextAlign.start,
      this.maxLengh,
      this.errorText,
      this.controller,
      this.activeBorderColor = Colors.transparent,
      this.borderRadius = 10.0,
      this.borderColor = Colors.transparent,
      this.backgroundColor = const Color(0xffEFEFEF),
      this.maxLines = 1,
      this.minLines = 1,
      this.prefixIcon,
      this.suffixIcon,
      this.suffixWidget,
      this.prefixWidget,
      this.inputDecoration,
      this.contentPadding,
      this.textdirection,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      maxLength: maxLengh,
      controller: controller,
      maxLines: maxLines,
      minLines: minLines,
      autofocus: !floatingHint,
      expands: expanded,
      textAlign: textalign,
      textDirection: textdirection,
      decoration: inputDecoration ??
          InputDecoration(
              floatingLabelBehavior: FloatingLabelBehavior.never,
              contentPadding: contentPadding ?? const EdgeInsets.all(16),
              filled: true,
              fillColor: backgroundColor.withOpacity(0.4),
              prefixIcon: prefixIcon,
              suffix: suffixWidget,
              prefix: prefixWidget,
              suffixIcon: suffixIcon,
              errorText: errorText,
              label: TextWidget(
                title: label,
                color: primaryColor,
              ),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: activeBorderColor, width: 1.0),
                  borderRadius:
                      BorderRadius.all(Radius.circular(borderRadius))),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: borderColor, width: 1.0),
                  borderRadius:
                      BorderRadius.all(Radius.circular(borderRadius))),
              errorBorder: OutlineInputBorder(
                  borderSide:
                      const BorderSide(color: Colors.transparent, width: 1.0),
                  borderRadius:
                      BorderRadius.all(Radius.circular(borderRadius))),
              errorMaxLines: 3,
              isDense: true,
              labelStyle: const TextStyle(color: primaryColor),
              hintStyle: const TextStyle(color: primaryColor, fontSize: 12),
              hintText: hintText),
      keyboardType: type,
      obscureText: password,
      onChanged: onChanged,
      onSaved: onSaved,
      validator: validator,
      style: const TextStyle(color: primaryColor, fontSize: 14),
    );
  }
}
