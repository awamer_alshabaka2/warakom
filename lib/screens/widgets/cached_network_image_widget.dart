import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:waraqukum/screens/widgets/shimmer_loading_skeleton.dart';

class CachedNetworkImageWidget extends StatelessWidget {
  const CachedNetworkImageWidget({
    super.key,
    required this.image,
    required this.height,
    this.width = double.infinity,
    this.borderRadius,
  });

  final String image;
  final double width;
  final double height;
  final BorderRadiusGeometry? borderRadius;

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      decoration: BoxDecoration(
        borderRadius: borderRadius ?? BorderRadius.zero,
      ),
      child: CachedNetworkImage(
        imageUrl: image,
        width: width,
        fit: BoxFit.cover,
        height: height,
        placeholder: (context, url) => ShimmerLoading(
          height: height,
          shape: BoxShape.rectangle,
        ),
        errorWidget: (context, url, error) => ShimmerLoading(
          width: width,
          height: height,
          shape: BoxShape.rectangle,
        ),
      ),
    );
  }
}
