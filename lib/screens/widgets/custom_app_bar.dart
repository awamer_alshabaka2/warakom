import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class CustomAppBar extends StatelessWidget {
  final String title;
  final EdgeInsets? padding;
  final double? height;
  final bool isArrowBackIcon;
  final Widget? actionWidget;
  const CustomAppBar({
    super.key,
    required this.title,
    this.padding,
    this.height,
    this.isArrowBackIcon = true,
    this.actionWidget,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding ?? const EdgeInsets.only(top: 50, right: 12, left: 12),
      height: height ?? 120,
      decoration: const BoxDecoration(
        color: secondaryColor,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(30),
          bottomRight: Radius.circular(30),
        ),
      ),
      child: Row(
        children: [
          Expanded(
            child: isArrowBackIcon
                ? GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Image.asset(
                      "assets/images/arrow.png",
                      alignment: AlignmentDirectional.centerStart,
                      width: 24,
                      height: 24,
                      fit: BoxFit.contain,
                    ),
                  )
                : const SizedBox(),
          ),
          Expanded(
            flex: 2,
            child: TextWidget(
                title: title,
                color: Colors.white,
                fontSize: 16,
                textAlign: TextAlign.center,
                fontWeight: FontWeight.w500),
          ),
          Expanded(
            child: actionWidget ?? const SizedBox(),
          )
        ],
      ),
    );
  }
}
