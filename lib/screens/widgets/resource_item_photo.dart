import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/widgets/cached_network_image_widget.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class ResourceItemPhoto extends StatelessWidget {
  const ResourceItemPhoto({
    super.key,
    required this.image,
    this.height = 117,
    this.isFavouriteIcon = true,
  });

  final String image;
  final double height;
  final bool isFavouriteIcon;

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.vertical(top: Radius.circular(7)),
        // border: Border.all(color: const Color(0xFFF3F3F3)),
      ),
      child: Stack(children: [
        CachedNetworkImageWidget(image: image, height: height),
        Padding(
          padding:
              const EdgeInsetsDirectional.only(top: 14, start: 14, end: 14),
          child: Row(children: [
            isFavouriteIcon
                ? Container(
                    alignment: Alignment.center,
                    width: 32,
                    height: 21,
                    decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(4)),
                        color: Colors.white.withOpacity(0.52),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.white.withOpacity(0.52),
                            blurRadius: 47,
                          )
                        ]),
                    child: Image.asset(
                      "assets/images/heart.png",
                      width: 14,
                      height: 14,
                      fit: BoxFit.fill,
                    ),
                  )
                : const SizedBox(),
            const Spacer(),
            Container(
              padding: const EdgeInsets.all(4),
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(4)),
                  color: Colors.white.withOpacity(0.52),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.white.withOpacity(0.52),
                      blurRadius: 47,
                    )
                  ]),
              child: const Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.star,
                    size: 18,
                    color: blue,
                  ),
                  SizedBox(width: 4),
                  TextWidget(
                    title: "4.5",
                    color: blue,
                    fontSize: 13,
                    fontWeight: FontWeight.bold,
                  )
                ],
              ),
            )
          ]),
        ),
      ]),
    );
  }
}
