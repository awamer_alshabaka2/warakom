import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class ResourceSection2 extends StatelessWidget {
  final bool isCartScreen;
  final bool isFree;
  const ResourceSection2({
    super.key,
    this.isCartScreen = false,
    this.isFree = false,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsetsDirectional.only(top: 8, start: 12),
          child: TextWidget(
            title: "المرجع الكامل في التحكم الكهربائي الصناعي",
            color: Colors.black,
            fontSize: 12,
            fontWeight: FontWeight.w500,
          ),
        ),
        const Padding(
          padding: EdgeInsetsDirectional.only(top: 8, start: 8),
          child: TextWidget(
            title: "تفارير - Pdf",
            color: dimGray,
            fontSize: 12,
            fontWeight: FontWeight.w500,
          ),
        ),
        isCartScreen
            ? const Padding(
                padding:
                    EdgeInsetsDirectional.only(top: 8, start: 8, bottom: 8),
                child: TextWidget(
                  title: "PDF (5.03) MB",
                  color: dimGray,
                  fontSize: 12,
                  fontWeight: FontWeight.w500,
                ),
              )
            : const Padding(
                padding:
                    EdgeInsetsDirectional.only(top: 8, start: 8, bottom: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    TextWidget(
                      title: "150 ر.س",
                      color: Color(0xff737373),
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                    ),
                    SizedBox(width: 8),
                    Expanded(
                      child: TextWidget(
                        title: "150 ر.س",
                        color: Color(0xff737373),
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        isOffer: true,
                      ),
                    ),
                  ],
                ),
              ),
        isFree
            ? Padding(
                padding: const EdgeInsetsDirectional.only(start: 8, end: 24),
                child: Row(
                  children: [
                    const TextWidget(
                      title: "مجانًا",
                      fontWeight: FontWeight.bold,
                      color: secondaryColor,
                    ),
                    const Spacer(),
                    Image.asset(
                      "assets/images/bin.png",
                      width: 16,
                      height: 16,
                      fit: BoxFit.fill,
                    ),
                  ],
                ),
              )
            : const SizedBox(),
      ],
    );
  }
}
