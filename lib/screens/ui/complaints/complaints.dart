import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/core/utils/validation.dart';
import 'package:waraqukum/screens/widgets/button_widget.dart';
import 'package:waraqukum/screens/widgets/custom_app_bar.dart';
import 'package:waraqukum/screens/widgets/text_field_widget.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class ComplaintsScreen extends StatelessWidget {
  const ComplaintsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        const CustomAppBar(title: "الشكاوى والمقترحات"),
        Expanded(
          child: SingleChildScrollView(
            child: Column(children: [
              const SizedBox(height: 32),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const TextWidget(
                      title: "الأسم",
                      fontSize: 14,
                      color: primaryColor,
                      fontWeight: FontWeight.bold,
                    ),
                    const SizedBox(height: 14),
                    TextFieldWidget(
                      label: "أدخل الأسم",
                      validator: Validation().defaultValidation,
                    ),
                    const SizedBox(height: 14),
                    const TextWidget(
                      title: "البريد الالكتروني",
                      fontSize: 14,
                      color: primaryColor,
                      fontWeight: FontWeight.bold,
                    ),
                    const SizedBox(height: 14),
                    TextFieldWidget(
                      label: " أدخل البريد الالكتروني",
                      validator: Validation().defaultValidation,
                    ),
                    const SizedBox(height: 14),
                    const TextWidget(
                      title: "محتوى الشكوى",
                      fontSize: 14,
                      color: primaryColor,
                      fontWeight: FontWeight.bold,
                    ),
                    const SizedBox(height: 14),
                    TextFieldWidget(
                      validator: Validation().defaultValidation,
                      minLines: 6,
                      maxLines: 8,
                    ),
                    const SizedBox(height: 185),
                    Align(
                      alignment: Alignment.center,
                      child: ButtonWidget(
                        title: "إرسال",
                        onTap: () {},
                      ),
                    ),
                  ],
                ),
              ),
            ]),
          ),
        )
      ],
    ));
  }
}
