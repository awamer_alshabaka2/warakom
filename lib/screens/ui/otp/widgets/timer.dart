// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class TimerWidget extends StatefulWidget {
  late int currentTime;
  TimerWidget({
    Key? key,
    required this.currentTime,
  }) : super(key: key);

  @override
  TimerWidgetState createState() => TimerWidgetState();
}

class TimerWidgetState extends State<TimerWidget> {
  late Timer _timer;
  @override
  void initState() {
    super.initState();

    // Set up a timer that runs every second
    _timer = Timer.periodic(const Duration(seconds: 1), _updateTime);
  }

  void _updateTime(Timer timer) {
    // String formattedTime = DateFormat('h:mm a').format(timenow);
    /// a is for am/pm and h is for 12 hour (H is for 24 hours)
    setState(() {
      // Update the current time
      if (widget.currentTime == 0) {
        widget.currentTime = widget.currentTime;
      } else {
        widget.currentTime = widget.currentTime - 1;
      }
    });
  }

  @override
  void dispose() {
    // Dispose of the timer when the widget is disposed
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        text: "الوقت المتبقي : ",
        style: const TextStyle(
          color: Color(0xff9A9A9A),
          fontSize: 14,
          fontFamily: 'URW',
          fontWeight: FontWeight.w500,
        ),
        children: [
          TextSpan(
            text: DateFormat('mm:ss')
                .format(DateTime(0, 0, 0, 0, 0, widget.currentTime)),
            style: const TextStyle(
              color: Color(0xff2D7679),
              fontSize: 14,
              fontWeight: FontWeight.w500,
            ),
          )
        ],
      ),
    );
  }
}
