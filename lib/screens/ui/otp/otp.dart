// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:waraqukum/core/utils/utils.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/core/utils/validation.dart';
import 'package:waraqukum/screens/ui/home/home.dart';
import 'package:waraqukum/screens/ui/otp/widgets/timer.dart';
import 'package:waraqukum/screens/widgets/button_widget.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class Otp extends StatefulWidget {
  const Otp({
    Key? key,
  }) : super(key: key);

  @override
  State<Otp> createState() => _OtpState();
}

class _OtpState extends State<Otp> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        Container(
          height: 120,
          decoration: const BoxDecoration(
            color: secondaryColor,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.only(bottom: 24),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    children: [
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Image.asset(
                            "assets/images/arrow.png",
                            alignment: AlignmentDirectional.centerStart,
                            width: 24,
                            height: 24,
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                      const Expanded(
                        child: TextWidget(
                            title: "تفعيل الحساب",
                            color: Colors.white,
                            fontSize: 16,
                            textAlign: TextAlign.center,
                            fontWeight: FontWeight.w500),
                      ),
                      const Expanded(
                        child: SizedBox(),
                      )
                    ],
                  )),
            ),
          ),
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Form(
              key: formKey,
              child: Column(children: [
                const SizedBox(height: 36),
                Image.asset(
                  "assets/images/logo.png",
                  width: 240,
                  height: 48,
                  fit: BoxFit.contain,
                ),
                const SizedBox(height: 24),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    children: [
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: TextWidget(
                          title:
                              "هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد م",
                          fontSize: 14,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      const SizedBox(height: 36),
                      Directionality(
                        textDirection: TextDirection.ltr,
                        child: PinCodeTextField(
                          backgroundColor: Colors.transparent,
                          appContext: context,
                          length: 4,
                          animationType: AnimationType.fade,
                          validator: (v) {
                            if (v != null && v.length < 4) {
                              return "هذا الحقل مطلوب";
                            } else {
                              return null;
                            }
                          },
                          blinkWhenObscuring: true,
                          pinTheme: PinTheme(
                            shape: PinCodeFieldShape.box,
                            fieldOuterPadding:
                                const EdgeInsets.symmetric(horizontal: 16),
                            fieldHeight: 54,
                            fieldWidth: 60,
                            borderRadius: BorderRadius.circular(8.0),
                            activeFillColor: offWhite,
                            activeColor: offWhite,
                            selectedColor: secondaryColor.withOpacity(0.5),
                            inactiveFillColor: offWhite,
                            inactiveColor: offWhite,
                            selectedFillColor: offWhite,
                          ),
                          cursorColor: secondaryColor,
                          errorTextSpace: 25,
                          errorTextDirection: TextDirection.rtl,
                          animationDuration: const Duration(milliseconds: 300),
                          enableActiveFill: true,
                          keyboardType: TextInputType.number,
                          onCompleted: Validation().defaultValidation,
                          onChanged: (_) {},
                        ),
                      ),
                      const SizedBox(height: 26),
                      Row(
                        children: [
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                setState(() {});
                              },
                              child: RichText(
                                text: const TextSpan(
                                  text: "لم تستلم الكود؟ ",
                                  style: TextStyle(
                                    color: Color(0xff9A9A9A),
                                    fontSize: 14,
                                    fontFamily: 'URW',
                                    fontWeight: FontWeight.w500,
                                  ),
                                  children: [
                                    TextSpan(
                                      text: "أعد الارسال",
                                      style: TextStyle(
                                        color: Color(0xff2D7679),
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          TimerWidget(currentTime: 20),
                        ],
                      ),
                      const SizedBox(height: 46),
                      ButtonWidget(
                          title: "الدخول",
                          onTap: () {
                            if (formKey.currentState!.validate()) {
                              Utils.openScreen(context, const HomeScreen());
                            }
                          }),
                    ],
                  ),
                )
              ]),
            ),
          ),
        )
      ],
    ));
  }
}
