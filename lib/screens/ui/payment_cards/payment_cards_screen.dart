import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/core/utils/utils.dart';
import 'package:waraqukum/screens/ui/add_card/add_payment_card.dart';
import 'package:waraqukum/screens/ui/payment_cards/widgets/payment_card_widget.dart';
import 'package:waraqukum/screens/widgets/button_widget.dart';
import 'package:waraqukum/screens/widgets/custom_app_bar.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class PaymentCardsScreen extends StatelessWidget {
  const PaymentCardsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        const CustomAppBar(title: "بطاقات الدفع"),
        Expanded(
          child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 28, vertical: 30),
              child: Column(children: [
                const PaymentCardWidget(),
                const SizedBox(height: 20),
                const PaymentCardWidget(
                  color: darkGray,
                ),
                const SizedBox(height: 60),
                ButtonWidget(
                  onTap: () {
                    Utils.openScreen(context, AddCardScreen());
                  },
                  child: const Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.add,
                        color: Colors.white,
                      ),
                      SizedBox(width: 8),
                      TextWidget(
                        title: "أضف بطاقة إئتمان جديدة",
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                      )
                    ],
                  ),
                )
              ])),
        )
      ],
    ));
  }
}
