// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';

import 'package:waraqukum/screens/widgets/text_widget.dart';

class PaymentCardWidget extends StatelessWidget {
  final Color? color;
  const PaymentCardWidget({
    Key? key,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 28,
      ),
      width: 320,
      height: 158,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: color,
        gradient: color == null
            ? const LinearGradient(
                colors: [
                  Color(0xff35BB80),
                  secondaryColor,
                ],
              )
            : null,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Padding(
            padding: const EdgeInsetsDirectional.only(top: 46),
            child: Image.asset(
              "assets/images/sim.png",
              width: 40,
              height: 30,
              fit: BoxFit.fill,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: Row(children: [
              Image.asset(
                "assets/images/mastercard.png",
                width: 50,
                height: 30,
                fit: BoxFit.fill,
              ),
              const Expanded(
                  child: TextWidget(
                title: "***** 1235",
                color: Colors.white,
                fontSize: 16,
                textDirection: TextDirection.ltr,
              )),
            ]),
          )
        ],
      ),
    );
  }
}
