import 'package:flutter_bloc/flutter_bloc.dart';

part 'invoices_state.dart';

class InvoicesCubit extends Cubit<InvoicesState> {
  InvoicesCubit() : super(InvoicesInitial());
  static InvoicesCubit get(context) => BlocProvider.of(context);
}
