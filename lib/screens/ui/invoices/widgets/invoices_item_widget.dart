import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class InvoicesItemWidget extends StatelessWidget {
  const InvoicesItemWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Column(children: [
          Container(
            padding: const EdgeInsets.all(16),
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8), topRight: Radius.circular(8)),
                color: offWhite),
            child: Row(
              children: [
                Image.asset(
                  "assets/images/file.png",
                  width: 16,
                  height: 16,
                  fit: BoxFit.fill,
                ),
                const SizedBox(width: 8),
                const TextWidget(
                  title: "رقم الفاتورة",
                  color: Colors.black,
                ),
                const Spacer(),
                const TextWidget(
                  title: "#12345",
                  color: Colors.black,
                ),
              ],
            ),
          ),
          const Padding(
            padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
            child: Column(
              children: [
                Row(children: [
                  TextWidget(
                    title: "تاريخ الطلب",
                    color: middleGray,
                  ),
                  Spacer(),
                  TextWidget(
                    title: "2022-12-12",
                    color: Colors.black,
                  )
                ]),
                SizedBox(height: 10),
                Row(children: [
                  TextWidget(
                    title: "تاريخ الطلب",
                    color: middleGray,
                  ),
                  Spacer(),
                  TextWidget(
                    title: "2022-12-12",
                    color: Colors.black,
                  )
                ]),
              ],
            ),
          ),
          Container(
              padding: const EdgeInsets.symmetric(vertical: 12),
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(8),
                      bottomRight: Radius.circular(8)),
                  color: secondaryColor),
              child:
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                const TextWidget(
                  title: "عرض الفاتورة",
                  color: Colors.white,
                  fontWeight: FontWeight.w500,
                ),
                const SizedBox(width: 16),
                Container(
                  padding: const EdgeInsets.all(8),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white.withOpacity(0.21)),
                  child: const Icon(
                    Icons.arrow_back_ios_new_rounded,
                    color: Colors.white,
                    size: 16,
                  ),
                )
              ]))
        ]),
      ],
    );
  }
}
