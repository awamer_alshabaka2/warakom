import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/ui/invoices/cubit/invoices_cubit.dart';
import 'package:waraqukum/screens/ui/invoices/widgets/invoices_item_widget.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class InvoicesScreen extends StatelessWidget {
  const InvoicesScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        Container(
          padding: const EdgeInsets.only(bottom: 24, right: 12, left: 12),
          height: 120,
          decoration: const BoxDecoration(
            color: secondaryColor,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30),
            ),
          ),
          child: Align(
              alignment: Alignment.bottomCenter,
              child: Row(
                children: [
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Image.asset(
                        "assets/images/arrow.png",
                        alignment: AlignmentDirectional.centerStart,
                        width: 24,
                        height: 24,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  const Expanded(
                    flex: 2,
                    child: TextWidget(
                        title: "الفواتير",
                        color: Colors.white,
                        fontSize: 16,
                        textAlign: TextAlign.center,
                        fontWeight: FontWeight.w500),
                  ),
                  const Expanded(
                    child: SizedBox(),
                  )
                ],
              )),
        ),
        Expanded(
          child: BlocProvider(
            create: (context) => InvoicesCubit(),
            child: BlocConsumer<InvoicesCubit, InvoicesState>(
              listener: (context, state) {},
              builder: (context, state) {
                return Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  child: ListView.separated(
                      itemBuilder: (context, index) =>
                          const InvoicesItemWidget(),
                      separatorBuilder: (context, index) => const SizedBox(
                            height: 20,
                          ),
                      itemCount: 15),
                );
              },
            ),
          ),
        )
      ],
    ));
  }
}
