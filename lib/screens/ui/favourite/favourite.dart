import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/widgets/resource_widget.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class FavouriteScren extends StatelessWidget {
  static String image =
      "https://images.pexels.com/photos/2599488/pexels-photo-2599488.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1";
  const FavouriteScren({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        Container(
          padding: const EdgeInsets.only(bottom: 24, right: 12, left: 12),
          height: 120,
          decoration: const BoxDecoration(
            color: secondaryColor,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30),
            ),
          ),
          child: Align(
              alignment: Alignment.bottomCenter,
              child: Row(
                children: [
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Image.asset(
                        "assets/images/arrow.png",
                        alignment: AlignmentDirectional.centerStart,
                        width: 24,
                        height: 24,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  const Expanded(
                    flex: 2,
                    child: TextWidget(
                        title: "قائمة المفضلة",
                        color: Colors.white,
                        fontSize: 16,
                        textAlign: TextAlign.center,
                        fontWeight: FontWeight.w500),
                  ),
                  const Expanded(
                    child: SizedBox(),
                  )
                ],
              )),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 28),
            child: GridView.builder(
              shrinkWrap: true,
              itemCount: 20,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 10,
                // mainAxisSpacing: 10,
                childAspectRatio: 160 / 270,
              ),
              itemBuilder: (context, index) {
                return ResourceWidget(
                  image: image,
                  index: index.toString(),
                );
              },
            ),
          ),
        )
      ],
    ));
  }
}
