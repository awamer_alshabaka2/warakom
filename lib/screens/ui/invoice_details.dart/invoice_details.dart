import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class InvoiceDetails extends StatelessWidget {
  const InvoiceDetails({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 36),
          height: 120,
          decoration: const BoxDecoration(
            color: secondaryColor,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30),
            ),
          ),
          child: const Padding(
            padding: EdgeInsets.only(bottom: 24),
            child: Align(
                alignment: Alignment.bottomCenter,
                child: TextWidget(
                    title: "فاتورة القيمة المضافة",
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.w500)),
          ),
        ),
        Expanded(
            child: SingleChildScrollView(
          child: Column(children: [
            Image.asset(
              "assets/images/logo.png",
              width: 240,
              height: 48,
              fit: BoxFit.contain,
            ),
            const SizedBox(height: 36),
            const Column(
              children: [
                Row(
                  children: [
                    TextWidget(
                      title: "الرقم الضريبي",
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                    ),
                    Spacer(),
                    TextWidget(
                      title: "22145544565",
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                    ),
                  ],
                ),
              ],
            )
          ]),
        ))
      ],
    ));
  }
}
