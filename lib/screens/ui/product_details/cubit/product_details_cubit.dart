import 'package:flutter_bloc/flutter_bloc.dart';

part 'product_details_state.dart';

enum RateButton {
  rateButton,
  detailsButton,
}

class ProductDetailsCubit extends Cubit<ProductDetailsState> {
  ProductDetailsCubit() : super(ProductDetailsInitial());
  static ProductDetailsCubit get(context) => BlocProvider.of(context);
  RateButton buttonChosed = RateButton.detailsButton;

  changeRateButton(RateButton button) {
    buttonChosed = button;
    emit(ChangeRateButtonState());
  }
}
