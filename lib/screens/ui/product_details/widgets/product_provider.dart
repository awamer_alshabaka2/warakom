import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/widgets/cached_network_image_widget.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class ProductProvider extends StatelessWidget {
  final String index;
  const ProductProvider({
    super.key,
    required this.index,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16),
      child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Hero(
          tag: index,
          child: CachedNetworkImageWidget(
            image:
                "https://images.pexels.com/photos/2599488/pexels-photo-2599488.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
            height: 64,
            width: 64,
            borderRadius: BorderRadius.circular(4),
          ),
        ),
        const SizedBox(width: 20),
        Expanded(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const TextWidget(
              title: "منى المنشاوي",
              fontWeight: FontWeight.w500,
              color: Colors.black,
            ),
            const SizedBox(height: 10),
            Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const TextWidget(
                  title: "500 متابع",
                  fontSize: 12,
                  color: middleGray,
                ),
                const SizedBox(width: 12),
                Image.asset(
                  "assets/images/starr.png",
                  height: 12,
                  width: 12,
                  fit: BoxFit.fill,
                ),
                const SizedBox(width: 6),
                const TextWidget(
                  title: "4.5",
                  fontSize: 12,
                  color: middleGray,
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Image.asset(
                        "assets/images/unfollow.png",
                        width: 16,
                        height: 16,
                        fit: BoxFit.fill,
                      ),
                      const SizedBox(width: 8),
                      const TextWidget(title: "متابعة")
                    ],
                  ),
                  Row(
                    children: [
                      Image.asset(
                        "assets/images/report.png",
                        width: 10,
                        height: 10,
                        fit: BoxFit.fill,
                      ),
                      const SizedBox(width: 8),
                      const TextWidget(title: "الإبلاغ")
                    ],
                  )
                ],
              ),
            )
          ],
        ))
      ]),
    );
  }
}
