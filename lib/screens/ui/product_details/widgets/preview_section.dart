import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/widgets/button_widget.dart';
import 'package:waraqukum/screens/widgets/resource_item_photo.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class PreviewSection extends StatelessWidget {
  const PreviewSection({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4.0),
      child: Column(
        children: [
          const ResourceItemPhoto(
            image:
                "https://images.pexels.com/photos/2599488/pexels-photo-2599488.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
          ),
          const Padding(
            padding: EdgeInsets.symmetric(vertical: 12.0),
            child: TextWidget(
              title: "المرجع الكامل في التحكم الكهربائي الصناعي",
              fontSize: 13,
              fontWeight: FontWeight.w500,
              color: Colors.black,
            ),
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              const Expanded(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextWidget(
                      title: "150 ر.س",
                      color: Color(0xff737373),
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                    ),
                    SizedBox(width: 8),
                    TextWidget(
                      title: "150 ر.س",
                      color: Color(0xff737373),
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      isOffer: true,
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      "assets/images/star.png",
                      height: 18,
                      width: 18,
                      fit: BoxFit.fill,
                    ),
                    const SizedBox(width: 6),
                    const TextWidget(
                      title: "4.5",
                      fontSize: 13,
                      color: middleGray,
                      fontWeight: FontWeight.bold,
                    )
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Image.asset(
                      "assets/images/heartt.png",
                      height: 18,
                      width: 18,
                      fit: BoxFit.fill,
                    ),
                    const SizedBox(width: 6),
                    const TextWidget(
                      title: "أضف المفضلة",
                      fontSize: 11,
                      color: secondaryColor,
                      fontWeight: FontWeight.w500,
                    )
                  ],
                ),
              ),
            ],
          ),
          const Padding(
            padding: EdgeInsets.only(top: 10, bottom: 18),
            child: ButtonWidget(
              width: double.infinity,
              title: "معاينة",
            ),
          )
        ],
      ),
    );
  }
}
