import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/widgets/shimmer_loading_skeleton.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class PeopleRates extends StatelessWidget {
  const PeopleRates({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CachedNetworkImage(
            imageUrl:
                "https://images.pexels.com/photos/2599488/pexels-photo-2599488.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
            imageBuilder: (context, imageProvider) => const CircleAvatar(
              backgroundImage: CachedNetworkImageProvider(
                "https://images.pexels.com/photos/2599488/pexels-photo-2599488.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
              ),
              radius: 20,
            ),
            placeholder: (context, url) => const ShimmerLoading(
              width: 30,
              height: 30,
              shape: BoxShape.circle,
            ),
            errorWidget: (context, url, error) => const ShimmerLoading(
              width: 30,
              height: 30,
              shape: BoxShape.circle,
            ),
          ),
          const SizedBox(width: 10),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const TextWidget(
                  title: "منى المنشاوي",
                  color: Colors.black,
                ),
                const SizedBox(height: 12),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    for (int i = 0; i < 5; i++)
                      Image.asset(
                        "assets/images/star.png",
                        height: 12,
                        width: 12,
                        fit: BoxFit.fill,
                      ),
                    const SizedBox(width: 6),
                    const TextWidget(
                      title: "4.9",
                      fontSize: 13,
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                    )
                  ],
                )
              ],
            ),
          ),
          const TextWidget(
            title: "10 مارس,2022",
            color: middleGray,
          )
        ],
      ),
      const SizedBox(height: 20),
      const Padding(
        padding: EdgeInsets.only(bottom: 28),
        child: TextWidget(
          title:
              "هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص ",
          fontSize: 12,
          color: Color(0xff193637),
          maxLines: null,
        ),
      ),
    ]);
  }
}
