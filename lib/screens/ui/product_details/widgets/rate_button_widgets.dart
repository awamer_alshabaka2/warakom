import 'package:flutter/material.dart';
import 'package:waraqukum/screens/ui/product_details/widgets/people_rates.dart';
import 'package:waraqukum/screens/ui/product_details/widgets/rate_number.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class RateButtonWidgets extends StatelessWidget {
  const RateButtonWidgets({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            for (int i = 0; i < 5; i++)
              Image.asset(
                "assets/images/star.png",
                height: 18,
                width: 18,
                fit: BoxFit.fill,
              ),
            const SizedBox(width: 6),
            const TextWidget(
              title: "4.9",
              fontSize: 22,
              color: Colors.black,
              fontWeight: FontWeight.bold,
            )
          ],
        ),
        const SizedBox(height: 8),
        const TextWidget(title: "200 تقييما"),
        const SizedBox(height: 20),
        const RatesNumber(
          starNumber: 5,
          rateNumber: 112,
          percent: 0.5,
        ),
        const RatesNumber(
          starNumber: 4,
          rateNumber: 24,
          percent: 0.7,
        ),
        const RatesNumber(
          starNumber: 3,
          rateNumber: 49,
          percent: 0.3,
        ),
        const RatesNumber(
          starNumber: 2,
          rateNumber: 220,
          percent: 0.1,
        ),
        const RatesNumber(
          starNumber: 1,
          rateNumber: 220,
          percent: 0.4,
        ),
        const SizedBox(height: 36),
        const PeopleRates(),
        const PeopleRates(),
        const PeopleRates(),
      ],
    );
  }
}
