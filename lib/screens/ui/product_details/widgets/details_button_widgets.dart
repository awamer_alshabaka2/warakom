import 'package:flutter/material.dart';
import 'package:waraqukum/screens/ui/product_details/product_details.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class DetailsButtonWidgets extends StatelessWidget {
  const DetailsButtonWidgets({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        const ColumnOfTextProductDetails(
          upTitle: "المرحلة الدراسية",
          downTitle: "صف أول ثانوي-صف ثان ثانوي",
        ),
        const SizedBox(height: 10),
        const ColumnOfTextProductDetails(
          upTitle: "المادة",
          downTitle: "الفيزياء",
        ),
        const SizedBox(height: 10),
        const Row(children: [
          Expanded(
            child: ColumnOfTextProductDetails(
              upTitle: "نوع الملف",
              downTitle: "PDF",
            ),
          ),
          Expanded(
            child: ColumnOfTextProductDetails(
              upTitle: "نوع المصدر",
              downTitle: "عروض تقديمية",
            ),
          ),
          Expanded(
            child: ColumnOfTextProductDetails(
              upTitle: "عدد الصفحات",
              downTitle: "صفحة",
            ),
          ),
        ]),
        const SizedBox(height: 20),
        const TextWidget(
          title: "الوصف",
          fontWeight: FontWeight.bold,
          color: Color(0xff193637),
        ),
        const SizedBox(height: 10),
        Container(
          padding: const EdgeInsets.all(12),
          decoration: BoxDecoration(
            color: const Color(0xffEFEFEF).withOpacity(0.30),
            borderRadius: BorderRadius.circular(10),
          ),
          child: const TextWidget(
            title:
                "هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان",
            maxLines: null,
            fontSize: 12,
            color: Color(0xff193637),
          ),
        ),
      ],
    );
  }
}
