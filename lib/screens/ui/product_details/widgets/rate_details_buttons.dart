import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/ui/product_details/cubit/product_details_cubit.dart';
import 'package:waraqukum/screens/widgets/button_widget.dart';

class RateAndDetailsButton extends StatelessWidget {
  final ProductDetailsCubit cubit;
  const RateAndDetailsButton({
    super.key,
    required this.cubit,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 16),
      child: Row(
        children: [
          Expanded(
              child: ButtonWidget(
            onTap: () => cubit.changeRateButton(RateButton.detailsButton),
            buttonColor: cubit.buttonChosed == RateButton.rateButton
                ? dimGray.withOpacity(0.10)
                : blue,
            textColor: cubit.buttonChosed == RateButton.rateButton
                ? darkGray
                : Colors.white,
            title: "التفاصيل",
          )),
          const SizedBox(width: 10),
          Expanded(
            child: ButtonWidget(
              onTap: () => cubit.changeRateButton(RateButton.rateButton),
              title: "التقييمات",
              buttonColor: cubit.buttonChosed == RateButton.rateButton
                  ? blue
                  : dimGray.withOpacity(0.10),
              textColor: cubit.buttonChosed == RateButton.rateButton
                  ? Colors.white
                  : darkGray,
            ),
          ),
        ],
      ),
    );
  }
}
