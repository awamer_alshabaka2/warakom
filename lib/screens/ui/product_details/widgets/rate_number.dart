import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class RatesNumber extends StatelessWidget {
  final int starNumber;
  final int rateNumber;
  final double percent;

  const RatesNumber({
    super.key,
    required this.starNumber,
    required this.rateNumber,
    required this.percent,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsetsDirectional.only(end: 14),
            child: TextWidget(
              title: starNumber.toString(),
              color: Colors.black,
              fontSize: 16,
            ),
          ),
          Padding(
            padding: const EdgeInsetsDirectional.only(end: 14),
            child: Image.asset(
              "assets/images/star.png",
              height: 18,
              width: 16,
              fit: BoxFit.fill,
            ),
          ),
          Expanded(
            child: RotatedBox(
              quarterTurns: 2,
              child: LinearPercentIndicator(
                padding: EdgeInsets.zero,
                lineHeight: 14.0,
                percent: percent,
                barRadius: const Radius.circular(9),
                backgroundColor: offWhite,
                progressColor: blue,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsetsDirectional.only(start: 18),
            child: TextWidget(
              title: rateNumber.toString(),
              color: Colors.black,
              fontSize: 18,
            ),
          ),
        ],
      ),
    );
  }
}
