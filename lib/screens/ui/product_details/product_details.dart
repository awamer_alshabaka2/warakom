// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/ui/product_details/cubit/product_details_cubit.dart';
import 'package:waraqukum/screens/ui/product_details/widgets/details_button_widgets.dart';
import 'package:waraqukum/screens/ui/product_details/widgets/preview_section.dart';
import 'package:waraqukum/screens/ui/product_details/widgets/product_provider.dart';
import 'package:waraqukum/screens/ui/product_details/widgets/rate_button_widgets.dart';
import 'package:waraqukum/screens/ui/product_details/widgets/rate_details_buttons.dart';
import 'package:waraqukum/screens/widgets/button_widget.dart';
import 'package:waraqukum/screens/widgets/custom_app_bar.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class ProductDetails extends StatelessWidget {
  final String index;
  ProductDetails({
    Key? key,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const CustomAppBar(
            title: "تفاصيل المنتج",
            isArrowBackIcon: true,
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: BlocProvider(
                create: (context) => ProductDetailsCubit(),
                child: BlocConsumer<ProductDetailsCubit, ProductDetailsState>(
                  listener: (context, state) {},
                  builder: (context, state) {
                    ProductDetailsCubit cubit =
                        ProductDetailsCubit.get(context);
                    return SingleChildScrollView(
                      child: Column(children: [
                        ProductProvider(index: index),
                        const PreviewSection(),
                        RateAndDetailsButton(cubit: cubit),
                        cubit.buttonChosed == RateButton.rateButton
                            ? const RateButtonWidgets()
                            : const DetailsButtonWidgets(),
                      ]),
                    );
                  },
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 16),
            child: ButtonWidget(
              width: double.infinity,
              borderRadius: BorderRadius.circular(10),
              child:
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                const TextWidget(
                  title: "أضف للسلة",
                  color: Colors.white,
                  fontSize: 18,
                ),
                const SizedBox(width: 8),
                Image.asset(
                  "assets/images/shopping-cart.png",
                  width: 18,
                  height: 14,
                  fit: BoxFit.fill,
                )
              ]),
            ),
          ),
        ],
      ),
    );
  }
}

class ColumnOfTextProductDetails extends StatelessWidget {
  final String upTitle;
  final String downTitle;
  const ColumnOfTextProductDetails({
    super.key,
    required this.upTitle,
    required this.downTitle,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextWidget(
          title: upTitle,
          color: dimGray,
          fontSize: 13,
        ),
        const SizedBox(height: 10),
        TextWidget(
          title: downTitle,
          fontSize: 13,
          color: Colors.black,
        ),
      ],
    );
  }
}
