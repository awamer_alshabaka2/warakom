import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/core/utils/validation.dart';
import 'package:waraqukum/screens/widgets/button_widget.dart';
import 'package:waraqukum/screens/widgets/custom_app_bar.dart';
import 'package:waraqukum/screens/widgets/text_field_widget.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class ContactUsScreen extends StatelessWidget {
  const ContactUsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        const CustomAppBar(title: "تواصل معنا"),
        Expanded(
          child: SingleChildScrollView(
            child: Column(children: [
              const SizedBox(height: 32),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: ButtonWidget(
                            title: "الأسئلة الشائعة",
                            buttonColor: const Color(0xffF0F0F0),
                            textColor: darkGray,
                            onTap: () {},
                          ),
                        ),
                        const SizedBox(width: 10),
                        Expanded(
                            child: ButtonWidget(
                          title: "تواصل معنا",
                          onTap: () {},
                          buttonColor: secondaryColor,
                          textColor: Colors.white,
                        )),
                      ],
                    ),
                    const SizedBox(height: 32),
                    const TextWidget(
                      title: "الأسم",
                      fontSize: 14,
                      color: primaryColor,
                      fontWeight: FontWeight.bold,
                    ),
                    const SizedBox(height: 14),
                    TextFieldWidget(
                      label: "أدخل الأسم",
                      validator: Validation().defaultValidation,
                    ),
                    const SizedBox(height: 14),
                    const TextWidget(
                      title: "رقم الجوال",
                      fontSize: 14,
                      color: primaryColor,
                      fontWeight: FontWeight.bold,
                    ),
                    const SizedBox(height: 14),
                    TextFieldWidget(
                      label: " أدخل رقم الجوال",
                      validator: Validation().defaultValidation,
                    ),
                    const SizedBox(height: 14),
                    const TextWidget(
                      title: "محتوى الرسالة",
                      fontSize: 14,
                      color: primaryColor,
                      fontWeight: FontWeight.bold,
                    ),
                    const SizedBox(height: 14),
                    Container(
                        padding: const EdgeInsets.all(12),
                        decoration: BoxDecoration(
                          color: const Color(0xffEFEFEF).withOpacity(0.30),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: const TextWidget(
                          title:
                              "هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان",
                          maxLines: null,
                        )),
                    const SizedBox(height: 14),
                    Align(
                      alignment: Alignment.center,
                      child: ButtonWidget(
                        title: "إرسال",
                        onTap: () {},
                      ),
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 46, bottom: 32),
                        child: TextWidget(
                          title: "أو عبر وسائل التواصل",
                          color: quartzColor.withOpacity(0.70),
                        ),
                      ),
                    ),
                    const Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SocialContainer(img: "assets/images/face.png"),
                        SocialContainer(img: "assets/images/icon.png"),
                        SocialContainer(img: "assets/images/twitter.png"),
                        SocialContainer(img: "assets/images/snapchat.png"),
                      ],
                    )
                  ],
                ),
              ),
            ]),
          ),
        )
      ],
    ));
  }
}

class SocialContainer extends StatelessWidget {
  final String img;
  const SocialContainer({
    super.key,
    required this.img,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsetsDirectional.only(end: 26),
      width: 48,
      height: 48,
      alignment: Alignment.center,
      decoration: const BoxDecoration(
        color: secondaryColor,
        shape: BoxShape.circle,
      ),
      child: Image.asset(
        img,
        width: 20,
        height: 20,
        fit: BoxFit.fill,
      ),
    );
  }
}
