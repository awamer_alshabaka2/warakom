import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:waraqukum/core/utils/cache_helper.dart';
import 'package:waraqukum/core/utils/statics.dart';
import 'package:waraqukum/core/utils/utils.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/core/utils/validation.dart';
import 'package:waraqukum/domain/models/user_model.dart';
import 'package:waraqukum/screens/ui/change_password/change_password.dart';
import 'package:waraqukum/screens/ui/profile/cubit/profile_cubit.dart';
import 'package:waraqukum/screens/widgets/button_widget.dart';
import 'package:waraqukum/screens/widgets/text_field_widget.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class ProfileScreen extends StatelessWidget {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        Container(
          height: 120,
          decoration: const BoxDecoration(
            color: secondaryColor,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.only(bottom: 24),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    children: [
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Image.asset(
                            "assets/images/arrow.png",
                            alignment: AlignmentDirectional.centerStart,
                            width: 24,
                            height: 24,
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                      const Expanded(
                        child: TextWidget(
                            title: "تعديل الحساب",
                            color: Colors.white,
                            fontSize: 16,
                            textAlign: TextAlign.center,
                            fontWeight: FontWeight.w500),
                      ),
                      const Expanded(
                        child: SizedBox(),
                      )
                    ],
                  )),
            ),
          ),
        ),
        Expanded(
          child: BlocProvider(
            create: (context) => ProfileCubit(),
            child: BlocConsumer<ProfileCubit, ProfileState>(
              listener: (context, state) {},
              builder: (context, state) {
                ProfileCubit cubit = ProfileCubit.get(context);
                return SingleChildScrollView(
                  child: Form(
                    key: formKey,
                    child: Column(children: [
                      const SizedBox(height: 36),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 30),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            GestureDetector(
                              onTap: () async {
                                final source =
                                    await Utils.showImageSource(context);
                                if (source == null) return;
                                cubit.pickImage(source);
                              },
                              child: Align(
                                alignment: Alignment.center,
                                child: Stack(
                                    alignment: AlignmentDirectional.bottomStart,
                                    children: [
                                      CircleAvatar(
                                        radius: 50,
                                        child: Container(
                                            clipBehavior: Clip.antiAlias,
                                            decoration: const BoxDecoration(
                                              shape: BoxShape.circle,
                                            ),
                                            child: cubit.profileImage == null &&
                                                    Utils.getUserModel()
                                                            ?.photo ==
                                                        null
                                                ? Image.asset(
                                                    'assets/images/logo.png',
                                                    width: 100,
                                                    height: 100,
                                                    fit: BoxFit.cover,
                                                  )
                                                : Image.file(
                                                    cubit.profileImage ??
                                                        File(
                                                            Utils.getUserModel()
                                                                    ?.photo ??
                                                                ''),
                                                    width: 100,
                                                    height: 100,
                                                    fit: BoxFit.cover,
                                                  )),
                                      ),
                                      Container(
                                          padding: const EdgeInsets.all(6),
                                          height: 30,
                                          width: 30,
                                          decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: secondaryColor,
                                          ),
                                          child: Image.asset(
                                            'assets/images/cam.png',
                                            width: 12,
                                            height: 10,
                                            fit: BoxFit.fill,
                                          ))
                                    ]),
                              ),
                            ),
                            const SizedBox(height: 22),
                            const TextWidget(
                              title: "الأسم",
                              fontSize: 14,
                              color: primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                            const SizedBox(height: 14),
                            TextFieldWidget(
                              controller: cubit.nameController,
                              label: "أدخل الأسم",
                              validator: Validation().defaultValidation,
                            ),
                            const SizedBox(height: 14),
                            const TextWidget(
                              title: "رقم الجوال",
                              fontSize: 14,
                              color: primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                            const SizedBox(height: 14),
                            TextFieldWidget(
                              controller: cubit.phoneController,
                              label: "أدخل رقم الجوال",
                              validator: Validation().defaultValidation,
                              suffixIcon: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Image.asset(
                                      "assets/images/saudi.png",
                                      height: 16,
                                      width: 24,
                                      fit: BoxFit.fill,
                                    ),
                                    const SizedBox(width: 4),
                                    const TextWidget(
                                      title: "+966",
                                    ),
                                    const SizedBox(width: 4),
                                    const Icon(
                                      Icons.arrow_drop_down_outlined,
                                    )
                                  ]),
                            ),
                            const SizedBox(height: 14),
                            const TextWidget(
                              title: "البريد الإلكتروني",
                              fontSize: 14,
                              color: primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                            const SizedBox(height: 14),
                            TextFieldWidget(
                              controller: cubit.emailController,
                              label: "أدخل البريد الإلكتروني",
                              validator: Validation().emailValidation,
                            ),
                            const SizedBox(height: 14),
                            const TextWidget(
                              title: "المرحلة الدراسية",
                              fontSize: 14,
                              color: primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                            const SizedBox(height: 14),
                            TextFieldWidget(
                              controller: cubit.levelController,
                              label: "أدخل المرحلة الدراسية",
                              validator: Validation().defaultValidation,
                            ),
                            const SizedBox(height: 32),
                            ButtonWidget(
                              onTap: () =>
                                  Utils.openScreen(context, ChangePassword()),
                              title: "تغير كلمة المرور",
                              buttonColor: Colors.white,
                              borderColor: secondaryColor,
                              textColor: secondaryColor,
                            ),
                            const SizedBox(height: 24),
                            ButtonWidget(
                                title: "حفظ",
                                onTap: () {
                                  if (formKey.currentState!.validate()) {
                                    UserModel? userModel = Utils.getUserModel();
                                    cubit.profileImage != null
                                        ? userModel?.photo =
                                            cubit.profileImage?.path
                                        : null;
                                    userModel?.name = cubit.nameController.text;
                                    userModel?.email =
                                        cubit.emailController.text;
                                    userModel?.phone =
                                        cubit.phoneController.text;
                                    userModel?.level =
                                        cubit.levelController.text;
                                    CacheHelper.saveData(
                                        key: Statics.userModel,
                                        value: jsonEncode(userModel?.toJson()));
                                    Navigator.pop(context);
                                  }
                                }),
                            const SizedBox(height: 32),
                          ],
                        ),
                      )
                    ]),
                  ),
                );
              },
            ),
          ),
        )
      ],
    ));
  }
}
