import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:waraqukum/core/utils/utils.dart';

part 'profile_state.dart';

class ProfileCubit extends Cubit<ProfileState> {
  ProfileCubit() : super(ProfileInitial());
  static ProfileCubit get(context) => BlocProvider.of(context);
  TextEditingController nameController =
      TextEditingController(text: Utils.getUserModel()?.name);
  TextEditingController phoneController =
      TextEditingController(text: Utils.getUserModel()?.phone);
  TextEditingController emailController =
      TextEditingController(text: Utils.getUserModel()?.email);

  TextEditingController levelController =
      TextEditingController(text: Utils.getUserModel()?.level);
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  bool isPassword = true;
  bool isPasswordConfirm = true;

  void changePasswordVisibility({required bool isPasswordConfirm}) {
    if (isPasswordConfirm) {
      this.isPasswordConfirm = !this.isPasswordConfirm;
    } else {
      isPassword = !isPassword;
    }
    emit(ChangePasswordVisibilityState());
  }

  File? profileImage;

  Future pickImage(ImageSource source) async {
    try {
      final image = await ImagePicker().pickImage(source: source);
      if (image == null) return;
      final imageTemporary = File(image.path);
      profileImage = imageTemporary;
      emit(UpdateImage());
    } catch (e) {
      debugPrint('Failed to pick image: $e');
    }
  }
}
