part of 'profile_cubit.dart';

abstract class ProfileState {}

final class ProfileInitial extends ProfileState {}

final class ChangePasswordVisibilityState extends ProfileState {}

final class UpdateImage extends ProfileState {}
