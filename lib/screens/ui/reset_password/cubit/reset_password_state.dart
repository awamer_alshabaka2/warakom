part of 'reset_password_cubit.dart';

abstract class ResetPasswordState {}

final class ResetPasswordInitial extends ResetPasswordState {}

final class ChangeVisibilitycode extends ResetPasswordState {}

final class ChangeNewPasswordVisibilityState extends ResetPasswordState {}

final class ChangeConfirmPasswordVisibilityState extends ResetPasswordState {}
