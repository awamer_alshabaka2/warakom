import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'reset_password_state.dart';

class ResetPasswordCubit extends Cubit<ResetPasswordState> {
  ResetPasswordCubit() : super(ResetPasswordInitial());
  static ResetPasswordCubit get(context) => BlocProvider.of(context);
  TextEditingController codeController = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  bool isVisibilitycode = true;
  bool isNewPassword = true;
  bool isNewPasswordConfirm = true;

  void changeVisibilitycode() {
    isVisibilitycode = !isVisibilitycode;
    emit(ChangeVisibilitycode());
  }

  void changenewPassword() {
    isNewPassword = !isNewPassword;
    emit(ChangeNewPasswordVisibilityState());
  }

  void changeConfirmPassword() {
    isNewPasswordConfirm = !isNewPasswordConfirm;
    emit(ChangeConfirmPasswordVisibilityState());
  }
}
