import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:waraqukum/core/utils/utils.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/core/utils/validation.dart';
import 'package:waraqukum/screens/ui/login/login_screen.dart';
import 'package:waraqukum/screens/ui/reset_password/cubit/reset_password_cubit.dart';
import 'package:waraqukum/screens/widgets/button_widget.dart';
import 'package:waraqukum/screens/widgets/text_field_widget.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class ResetPassword extends StatelessWidget {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  ResetPassword({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        Container(
          height: 120,
          decoration: const BoxDecoration(
            color: secondaryColor,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.only(bottom: 24),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    children: [
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Image.asset(
                            "assets/images/arrow.png",
                            alignment: AlignmentDirectional.centerStart,
                            width: 24,
                            height: 24,
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                      const Expanded(
                        flex: 2,
                        child: TextWidget(
                            title: "إستعادة كلمة المرور",
                            color: Colors.white,
                            fontSize: 16,
                            textAlign: TextAlign.center,
                            fontWeight: FontWeight.w500),
                      ),
                      const Expanded(
                        child: SizedBox(),
                      )
                    ],
                  )),
            ),
          ),
        ),
        Expanded(
          child: BlocProvider(
            create: (context) => ResetPasswordCubit(),
            child: BlocConsumer<ResetPasswordCubit, ResetPasswordState>(
              listener: (context, state) {},
              builder: (context, state) {
                ResetPasswordCubit cubit = ResetPasswordCubit.get(context);
                return SingleChildScrollView(
                  child: Form(
                    key: formKey,
                    child: Column(children: [
                      const SizedBox(height: 36),
                      Image.asset(
                        "assets/images/logo.png",
                        width: 240,
                        height: 48,
                        fit: BoxFit.contain,
                      ),
                      const SizedBox(height: 24),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 30),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Padding(
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              child: TextWidget(
                                title:
                                    "هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد م",
                                fontSize: 14,
                                textAlign: TextAlign.center,
                              ),
                            ),
                            const SizedBox(height: 36),
                            const TextWidget(
                              title: "الكود",
                              fontSize: 14,
                              color: primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                            const SizedBox(height: 10),
                            TextFieldWidget(
                              label: "الرجاء ادخال الكود المرسل اليك",
                              password: cubit.isVisibilitycode,
                              validator: Validation().defaultValidation,
                              suffixIcon: GestureDetector(
                                  onTap: () => cubit.changeVisibilitycode(),
                                  child: Icon(
                                    cubit.isVisibilitycode
                                        ? Icons.visibility_outlined
                                        : Icons.visibility_off_outlined,
                                    color: const Color(0xffBCBCBC),
                                    size: 22,
                                  )),
                            ),
                            const SizedBox(height: 14),
                            const TextWidget(
                              title: "كلمة المرور الجديدة",
                              fontSize: 14,
                              color: primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                            const SizedBox(height: 10),
                            TextFieldWidget(
                              controller: cubit.newPasswordController,
                              label: "كلمة المرور الجديدة",
                              password: cubit.isNewPassword,
                              validator: Validation().validatePassword,
                              suffixIcon: GestureDetector(
                                  onTap: () => cubit.changenewPassword(),
                                  child: Icon(
                                    cubit.isNewPassword
                                        ? Icons.visibility_outlined
                                        : Icons.visibility_off_outlined,
                                    color: const Color(0xffBCBCBC),
                                    size: 22,
                                  )),
                            ),
                            const SizedBox(height: 14),
                            const TextWidget(
                              title: "تأكيد كلمة المرور الجديدة",
                              fontSize: 14,
                              color: primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                            const SizedBox(height: 10),
                            TextFieldWidget(
                              label: "تأكيد كلمة المرور الجديدة",
                              validator: (value) {
                                return Validation().confirmPasswordValidation(
                                    value, cubit.newPasswordController.text);
                              },
                              password: cubit.isNewPasswordConfirm,
                              suffixIcon: GestureDetector(
                                  onTap: () => cubit.changeConfirmPassword(),
                                  child: Icon(
                                    cubit.isNewPasswordConfirm
                                        ? Icons.visibility_outlined
                                        : Icons.visibility_off_outlined,
                                    color: const Color(0xffBCBCBC),
                                    size: 22,
                                  )),
                            ),
                            const SizedBox(height: 46),
                            Align(
                              alignment: AlignmentDirectional.center,
                              child: ButtonWidget(
                                  title: "الدخول",
                                  onTap: () {
                                    if (formKey.currentState!.validate()) {
                                      Utils.openScreen(context, LoginScreen());
                                    }
                                  }),
                            ),
                          ],
                        ),
                      )
                    ]),
                  ),
                );
              },
            ),
          ),
        )
      ],
    ));
  }
}
