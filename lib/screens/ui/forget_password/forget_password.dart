import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/utils.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/core/utils/validation.dart';
import 'package:waraqukum/screens/ui/reset_password/reset_password.dart';
import 'package:waraqukum/screens/widgets/button_widget.dart';
import 'package:waraqukum/screens/widgets/text_field_widget.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class ForgetPassword extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  ForgetPassword({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        Container(
          height: 120,
          decoration: const BoxDecoration(
            color: secondaryColor,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.only(bottom: 24),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    children: [
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Image.asset(
                            "assets/images/arrow.png",
                            alignment: AlignmentDirectional.centerStart,
                            width: 24,
                            height: 24,
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                      const Expanded(
                        flex: 2,
                        child: TextWidget(
                            title: "إستعادة كلمة المرور",
                            color: Colors.white,
                            fontSize: 16,
                            textAlign: TextAlign.center,
                            fontWeight: FontWeight.w500),
                      ),
                      const Expanded(
                        child: SizedBox(),
                      )
                    ],
                  )),
            ),
          ),
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(children: [
                const SizedBox(height: 36),
                Image.asset(
                  "assets/images/logo.png",
                  width: 240,
                  height: 48,
                  fit: BoxFit.contain,
                ),
                const SizedBox(height: 24),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    children: [
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: TextWidget(
                          title:
                              "هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد م",
                          fontSize: 14,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      const SizedBox(height: 36),
                      const Align(
                        alignment: AlignmentDirectional.centerStart,
                        child: TextWidget(
                          title: "رقم الجوال",
                          fontSize: 14,
                          color: primaryColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const SizedBox(height: 10),
                      TextFieldWidget(
                        label: "أدخل رقم الجوال",
                        validator: Validation().defaultValidation,
                        suffixIcon:
                            Row(mainAxisSize: MainAxisSize.min, children: [
                          Image.asset(
                            "assets/images/saudi.png",
                            height: 16,
                            width: 24,
                            fit: BoxFit.fill,
                          ),
                          const SizedBox(width: 4),
                          const TextWidget(
                            title: "+966",
                          ),
                          const SizedBox(width: 4),
                          const Icon(
                            Icons.arrow_drop_down_outlined,
                          )
                        ]),
                      ),
                      const SizedBox(height: 46),
                      ButtonWidget(
                        title: "إرسال",
                        onTap: () {
                          if (_formKey.currentState!.validate()) {
                            Utils.openScreen(context, ResetPassword());
                          }
                        },
                      ),
                    ],
                  ),
                )
              ]),
            ),
          ),
        )
      ],
    ));
  }
}
