import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/ui/invoices/cubit/invoices_cubit.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class NotificationsScreen extends StatelessWidget {
  const NotificationsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        Container(
          padding: const EdgeInsets.only(bottom: 24, right: 12, left: 12),
          height: 120,
          decoration: const BoxDecoration(
            color: secondaryColor,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30),
            ),
          ),
          child: Align(
              alignment: Alignment.bottomCenter,
              child: Row(
                children: [
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Image.asset(
                        "assets/images/arrow.png",
                        alignment: AlignmentDirectional.centerStart,
                        width: 24,
                        height: 24,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  const Expanded(
                    flex: 2,
                    child: TextWidget(
                        title: "الاشعارات",
                        color: Colors.white,
                        fontSize: 16,
                        textAlign: TextAlign.center,
                        fontWeight: FontWeight.w500),
                  ),
                  const Expanded(
                    child: SizedBox(),
                  )
                ],
              )),
        ),
        Expanded(
          child: BlocProvider(
            create: (context) => InvoicesCubit(),
            child: BlocConsumer<InvoicesCubit, InvoicesState>(
              listener: (context, state) {},
              builder: (context, state) {
                return Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                  child: ListView.separated(
                      itemBuilder: (context, index) => Container(
                            height: 64,
                            padding: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(8),
                              boxShadow: const [
                                BoxShadow(
                                  offset: Offset(0, 2),
                                  color: Color(0xffEEEEEE),
                                  blurRadius: 8,
                                ),
                                BoxShadow(
                                  offset: Offset(0, 3),
                                  color: Color(0xffF9F9F9),
                                  blurRadius: 6,
                                ),
                              ],
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  alignment: Alignment.center,
                                  width: 40,
                                  height: 40,
                                  decoration: const BoxDecoration(
                                      color: Color(0xffDCEDE6),
                                      shape: BoxShape.circle),
                                  child: Image.asset(
                                    "assets/images/bell.png",
                                    width: 20,
                                    height: 24,
                                    fit: BoxFit.fill,
                                  ),
                                ),
                                const SizedBox(width: 16),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    const TextWidget(
                                      title:
                                          "هذا النص هو مثال لنص يمكن ان يستبدل بنص اخر",
                                      color: Color(0xff193637),
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500,
                                    ),
                                    const SizedBox(height: 8),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Image.asset(
                                          "assets/images/clock.png",
                                          width: 10,
                                          height: 10,
                                          fit: BoxFit.fill,
                                        ),
                                        const SizedBox(width: 4),
                                        const TextWidget(
                                          title: "منذ : 5 دقائق",
                                          color: Color(0xffBCBCBC),
                                          fontSize: 10,
                                        )
                                      ],
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                      separatorBuilder: (context, index) => const SizedBox(
                            height: 20,
                          ),
                      itemCount: 15),
                );
              },
            ),
          ),
        )
      ],
    ));
  }
}
