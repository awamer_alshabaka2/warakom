import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class TableItemWidget extends StatelessWidget {
  const TableItemWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      const Expanded(
          flex: 2,
          child: TextWidget(
            title: "منى المنشاوي",
            textAlign: TextAlign.center,
            color: Color(0xff161616),
          )),
      Expanded(
        child: Transform.flip(
          flipX: true,
          child: Image.asset(
            'assets/images/toggle.png',
            width: 22,
            height: 10,
            color: secondaryColor,
          ),
        ),
      ),
      Expanded(
          child: Center(
        child: Image.asset(
          "assets/images/deleteee.png",
          width: 20,
          height: 20,
          fit: BoxFit.fill,
        ),
      )),
    ]);
  }
}
