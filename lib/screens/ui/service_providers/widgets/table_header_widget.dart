import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class TableHeaderWidget extends StatelessWidget {
  const TableHeaderWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: offWhite,
        ),
        child: const Row(children: [
          Expanded(
              flex: 2,
              child: TextWidget(
                title: "إسم مقدم الخدمة",
                textAlign: TextAlign.center,
                color: Color(0xff161616),
              )),
          Expanded(
              child: TextWidget(
            title: "الإشعارات",
            textAlign: TextAlign.center,
            color: Color(0xff161616),
          )),
          Expanded(
              child: TextWidget(
            title: "حذف",
            textAlign: TextAlign.center,
            color: Color(0xff161616),
          )),
        ]));
  }
}
