import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/ui/service_providers/widgets/table_item_widget.dart';

class TableWidget extends StatelessWidget {
  const TableWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.vertical(bottom: Radius.circular(5)),
        color: Colors.white,
        border: Border(
          bottom: BorderSide(
            color: dimGray.withOpacity(0.30),
          ),
          top: BorderSide(color: dimGray.withOpacity(0.30), width: 0),
          left: BorderSide(
            color: dimGray.withOpacity(0.30),
          ),
          right: BorderSide(
            color: dimGray.withOpacity(0.30),
          ),
        ),
      ),
      child: ListView.separated(
        padding: EdgeInsets.zero,
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: 16,
        separatorBuilder: (context, index) => const Divider(),
        itemBuilder: (context, index) => const TableItemWidget(),
      ),
    );
  }
}
