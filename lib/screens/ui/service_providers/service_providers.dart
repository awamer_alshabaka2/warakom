import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/ui/service_providers/widgets/table_header_widget.dart';
import 'package:waraqukum/screens/ui/service_providers/widgets/table_widget.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class ServiceProviders extends StatelessWidget {
  const ServiceProviders({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        Container(
          padding: const EdgeInsets.only(bottom: 24, right: 12, left: 12),
          height: 120,
          decoration: const BoxDecoration(
            color: secondaryColor,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30),
            ),
          ),
          child: Align(
              alignment: Alignment.bottomCenter,
              child: Row(
                children: [
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Image.asset(
                        "assets/images/arrow.png",
                        alignment: AlignmentDirectional.centerStart,
                        width: 24,
                        height: 24,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  const Expanded(
                    flex: 2,
                    child: TextWidget(
                        title: "مقدمي الخدمة المتابعيين",
                        color: Colors.white,
                        fontSize: 16,
                        textAlign: TextAlign.center,
                        fontWeight: FontWeight.w500),
                  ),
                  const Expanded(
                    child: SizedBox(),
                  )
                ],
              )),
        ),
        const Expanded(
          child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 30),
              child: Column(children: [
                TableHeaderWidget(),
                TableWidget(),
              ])),
        )
      ],
    ));
  }
}
