
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/core/utils/validation.dart';
import 'package:waraqukum/screens/ui/add_card/cubit/add_card_cubit.dart';
import 'package:waraqukum/screens/widgets/button_widget.dart';
import 'package:waraqukum/screens/widgets/text_field_widget.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class AddCardScreen extends StatelessWidget {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  AddCardScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        Container(
          height: 120,
          decoration: const BoxDecoration(
            color: secondaryColor,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.only(bottom: 24),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    children: [
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Image.asset(
                            "assets/images/arrow.png",
                            alignment: AlignmentDirectional.centerStart,
                            width: 24,
                            height: 24,
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                      const Expanded(
                        child: TextWidget(
                            title: "إضافة بطاقة إئتمان",
                            color: Colors.white,
                            fontSize: 16,
                            textAlign: TextAlign.center,
                            fontWeight: FontWeight.w500),
                      ),
                      const Expanded(
                        child: SizedBox(),
                      )
                    ],
                  )),
            ),
          ),
        ),
        Expanded(
          child: BlocProvider(
            create: (context) => AddCardCubit(),
            child: BlocConsumer<AddCardCubit, AddCardState>(
              listener: (context, state) {},
              builder: (context, state) {
                AddCardCubit cubit = AddCardCubit.get(context);
                return SingleChildScrollView(
                  child: Form(
                    key: formKey,
                    child: Column(children: [
                      const SizedBox(height: 36),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 30),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            const TextWidget(
                              title: "رقم الجوال",
                              fontSize: 14,
                              color: primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                            const SizedBox(height: 14),
                            TextFieldWidget(
                              controller: cubit.phoneController,
                              label: "أدخل رقم الجوال",
                              validator: Validation().defaultValidation,
                              suffixIcon: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Image.asset(
                                      "assets/images/saudi.png",
                                      height: 16,
                                      width: 24,
                                      fit: BoxFit.fill,
                                    ),
                                    const SizedBox(width: 4),
                                    const TextWidget(
                                      title: "+966",
                                    ),
                                    const SizedBox(width: 4),
                                    const Icon(
                                      Icons.arrow_drop_down_outlined,
                                    )
                                  ]),
                            ),
                            const SizedBox(height: 14),
                            const TextWidget(
                              title: "البريد الإلكتروني",
                              fontSize: 14,
                              color: primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                            const SizedBox(height: 14),
                            TextFieldWidget(
                              controller: cubit.emailController,
                              label: "أدخل البريد الإلكتروني",
                              validator: Validation().emailValidation,
                            ),
                            const SizedBox(height: 14),
                            const TextWidget(
                              title: "كلمة المرور",
                              fontSize: 14,
                              color: primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                            const SizedBox(height: 10),
                            TextFieldWidget(
                              controller: cubit.passwordController,
                              label: "ادخل كلمة المرور",
                              validator: Validation().validatePassword,
                              password: cubit.isPassword,
                              suffixIcon: GestureDetector(
                                  onTap: () => cubit.changePasswordVisibility(),
                                  child: Icon(
                                    cubit.isPassword
                                        ? Icons.visibility_outlined
                                        : Icons.visibility_off_outlined,
                                    color: const Color(0xffBCBCBC),
                                    size: 22,
                                  )),
                            ),
                            const SizedBox(height: 32),
                            ButtonWidget(
                                title: "إضافة البطاقة",
                                onTap: () {
                                  if (formKey.currentState!.validate()) {}
                                }),
                            const SizedBox(height: 32),
                          ],
                        ),
                      )
                    ]),
                  ),
                );
              },
            ),
          ),
        )
      ],
    ));
  }
}
