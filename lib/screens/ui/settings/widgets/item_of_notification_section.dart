import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class ItemOfNotificationSection extends StatelessWidget {
  final String title;
  const ItemOfNotificationSection({
    super.key,
    required this.title,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 24),
      child: Row(children: [
        TextWidget(
          title: title,
          fontSize: 14,
          fontWeight: FontWeight.w500,
          color: quartzColor,
        ),
        const Spacer(),
        Transform.flip(
          flipX: false,
          child: Image.asset(
            'assets/images/toggle.png',
            width: 38,
            height: 18,
            color: secondaryColor,
          ),
        ),
      ]),
    );
  }
}
