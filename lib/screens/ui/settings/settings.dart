import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/ui/settings/widgets/item_of_notification_section.dart';
import 'package:waraqukum/screens/widgets/custom_app_bar.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

enum Season { arabic, english }

class SettingsScreen extends StatefulWidget {
  Season season = Season.arabic;
  SettingsScreen({super.key});

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        const CustomAppBar(title: "الإعدادات"),
        Expanded(
          child: SingleChildScrollView(
            child: Column(children: [
              const SizedBox(height: 32),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 18),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const TextWidget(
                        title:
                            "هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربي",
                        fontSize: 14,
                        textAlign: TextAlign.center),
                    const SizedBox(height: 28),
                    const TextWidget(
                      title: "إعدادات اللغة",
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                    radioWidgets(),
                    const TextWidget(
                      title: "إعدادات الإشعارات",
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                    const SizedBox(height: 20),
                    const ItemOfNotificationSection(title: "الإشعارات"),
                    const ItemOfNotificationSection(title: "إشعارات العروض"),
                    const ItemOfNotificationSection(
                        title: "اشعارات المتاجر المتابعة"),
                  ],
                ),
              ),
            ]),
          ),
        )
      ],
    ));
  }

  Padding radioWidgets() {
    return Padding(
      padding: const EdgeInsetsDirectional.only(top: 20, bottom: 50),
      child: Row(
        children: [
          Expanded(
            child: Row(
              children: [
                Radio(
                    value: Season.arabic,
                    groupValue: widget.season,
                    activeColor: secondaryColor,
                    onChanged: (Season? value) {
                      setState(() {
                        widget.season = value!;
                      });
                    }),
                const TextWidget(
                  title: "اللغة العربية",
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  color: quartzColor,
                ),
              ],
            ),
          ),
          Expanded(
            flex: 2,
            child: Row(
              children: [
                Radio(
                    value: Season.english,
                    groupValue: widget.season,
                    activeColor: secondaryColor,
                    onChanged: (Season? value) {
                      setState(() {
                        widget.season = value!;
                      });
                    }),
                const TextWidget(
                  title: "English",
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  color: quartzColor,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
