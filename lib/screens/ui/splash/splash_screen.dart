import 'package:animated_widgets/widgets/opacity_animated.dart';
import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/utils.dart';
import 'package:waraqukum/screens/ui/login/login_screen.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: OpacityAnimatedWidget.tween(
        opacityEnabled: 1,
        opacityDisabled: 0,
        duration: const Duration(milliseconds: 2000),
        enabled: true,
        animationFinished: (finished) async {
          // final token = await locator<DataManager>().getData(Statics.token);
          // if (token != null) {
          //   Utils.token = token;
          //   await Utils.getUser();
          // } else {
          //   Utils.token = token;
          // }
          Utils.openScreen(context, LoginScreen(), remove: true);
        },
        child: Image.asset(
          "assets/images/logo.png",
          width: 200,
          height: 200,
          fit: BoxFit.contain,
        ),
      ),
    ));
  }
}
