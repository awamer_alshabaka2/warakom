import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:waraqukum/core/utils/utils.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/core/utils/validation.dart';
import 'package:waraqukum/domain/models/user_model.dart';
import 'package:waraqukum/screens/ui/forget_password/forget_password.dart';
import 'package:waraqukum/screens/ui/home/home.dart';
import 'package:waraqukum/screens/ui/login/cubit/login_cubit.dart';
import 'package:waraqukum/screens/ui/sign_up/signup_screen.dart';
import 'package:waraqukum/screens/widgets/button_widget.dart';
import 'package:waraqukum/screens/widgets/custom_app_bar.dart';
import 'package:waraqukum/screens/widgets/text_field_widget.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class LoginScreen extends StatelessWidget {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        const CustomAppBar(title: "تسجيل دخول"),
        Expanded(
          child: BlocProvider(
            create: (context) => LoginCubit(),
            child: BlocConsumer<LoginCubit, LoginState>(
              listener: (context, state) {},
              builder: (context, state) {
                LoginCubit cubit = LoginCubit.get(context);
                return SingleChildScrollView(
                  child: Form(
                    key: formKey,
                    child: Column(children: [
                      const SizedBox(height: 36),
                      Image.asset(
                        "assets/images/logo.png",
                        width: 240,
                        height: 48,
                        fit: BoxFit.contain,
                      ),
                      const SizedBox(height: 50),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 30),
                        child: Column(
                          children: [
                            const Align(
                              alignment: AlignmentDirectional.centerStart,
                              child: TextWidget(
                                title: "رقم الجوال",
                                fontSize: 14,
                                color: primaryColor,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            const SizedBox(height: 10),
                            TextFieldWidget(
                              controller: cubit.phoneController,
                              label: "أدخل رقم الجوال",
                              validator: Validation().defaultValidation,
                              suffixIcon: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Image.asset(
                                      "assets/images/saudi.png",
                                      height: 16,
                                      width: 24,
                                      fit: BoxFit.fill,
                                    ),
                                    const SizedBox(width: 4),
                                    const TextWidget(
                                      title: "+966",
                                    ),
                                    const SizedBox(width: 4),
                                    const Icon(
                                      Icons.arrow_drop_down_outlined,
                                    )
                                  ]),
                            ),
                            const SizedBox(height: 20),
                            const Align(
                              alignment: AlignmentDirectional.centerStart,
                              child: TextWidget(
                                title: "كلمة المرور",
                                fontSize: 14,
                                color: primaryColor,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            const SizedBox(height: 10),
                            TextFieldWidget(
                              controller: cubit.passwordController,
                              label: "ادخل كلمة المرور",
                              validator: Validation().validatePassword,
                              password: cubit.isPassword,
                              suffixIcon: GestureDetector(
                                  onTap: () => cubit.changePasswordVisibility(),
                                  child: Icon(
                                    cubit.isPassword
                                        ? Icons.visibility_outlined
                                        : Icons.visibility_off_outlined,
                                    color: const Color(0xffBCBCBC),
                                    size: 22,
                                  )),
                            ),
                            const SizedBox(height: 8),
                            GestureDetector(
                              onTap: () =>
                                  Utils.openScreen(context, ForgetPassword()),
                              child: const Align(
                                alignment: AlignmentDirectional.centerEnd,
                                child: TextWidget(
                                  title: "هل نسيت كلمة المرور؟",
                                  color: secondaryColor,
                                  underLine: true,
                                ),
                              ),
                            ),
                            const SizedBox(height: 30),
                            ButtonWidget(
                              title: "تسجيل الدخول",
                              onTap: () {
                                if (formKey.currentState!.validate()) {
                                  UserModel? userModel = Utils.getUserModel();

                                  if (userModel != null &&
                                      userModel.phone ==
                                          cubit.phoneController.text &&
                                      userModel.password ==
                                          cubit.passwordController.text) {
                                    Utils.openScreen(
                                        context, const HomeScreen());
                                  } else {
                                    Utils.showMsg("البيانات غير صحيحة");
                                  }
                                }
                              },
                            ),
                            const SizedBox(height: 30),
                            Align(
                              alignment: AlignmentDirectional.center,
                              child: GestureDetector(
                                onTap: () =>
                                    Utils.openScreen(context, HomeScreen()),
                                child: TextWidget(
                                  title: "الدخول كزائر",
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                  color: secondaryColor,
                                  underLine: true,
                                ),
                              ),
                            ),
                            const SizedBox(height: 160),
                            GestureDetector(
                              onTap: () {
                                Utils.openScreen(context, SignUpScreen());
                              },
                              child: const Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  TextWidget(
                                    title: "لا يوجد لديك حساب ؟",
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    color: secondaryColor,
                                  ),
                                  TextWidget(
                                    title: " قم بإنشاء حساب",
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    color: secondaryColor,
                                    underLine: true,
                                  )
                                ],
                              ),
                            ),
                            const SizedBox(height: 50),
                          ],
                        ),
                      )
                    ]),
                  ),
                );
              },
            ),
          ),
        )
      ],
    ));
  }
}
