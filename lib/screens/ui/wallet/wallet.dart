import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/ui/wallet/cubit/wallet_cubit.dart';
import 'package:waraqukum/screens/ui/wallet/widgets/my_points.dart';
import 'package:waraqukum/screens/ui/wallet/widgets/the_wallet.dart';
import 'package:waraqukum/screens/widgets/button_widget.dart';
import 'package:waraqukum/screens/widgets/custom_app_bar.dart';

class WalletScreen extends StatelessWidget {
  const WalletScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        const CustomAppBar(title: "المحفظة"),
        Expanded(
          child: BlocProvider(
            create: (context) => WalletCubit(),
            child: BlocConsumer<WalletCubit, WalletState>(
              listener: (context, state) {},
              builder: (context, state) {
                WalletCubit cubit = WalletCubit.get(context);
                return SingleChildScrollView(
                  child: Column(children: [
                    const SizedBox(height: 32),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                  child: ButtonWidget(
                                      title: "المحفظة",
                                      buttonColor: cubit.isWallet
                                          ? secondaryColor
                                          : const Color(0xffF0F0F0),
                                      textColor: cubit.isWallet
                                          ? const Color.fromRGBO(
                                              255, 255, 255, 1)
                                          : darkGray,
                                      onTap: () {
                                        cubit.changeWallet(isWallet: true);
                                      })),
                              const SizedBox(width: 10),
                              Expanded(
                                  child: ButtonWidget(
                                title: "نقاطي",
                                onTap: () {
                                  cubit.changeWallet(isWallet: false);
                                },
                                buttonColor: cubit.isWallet
                                    ? const Color(0xffF0F0F0)
                                    : secondaryColor,
                                textColor:
                                    cubit.isWallet ? darkGray : Colors.white,
                              )),
                            ],
                          ),
                          const SizedBox(height: 32),
                          cubit.isWallet
                              ? TheWalletWidget(
                                  cubit: cubit,
                                )
                              : MyPointsWidget(
                                  cubit: cubit,
                                )
                        ],
                      ),
                    ),
                  ]),
                );
              },
            ),
          ),
        )
      ],
    ));
  }
}
