import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/ui/wallet/cubit/wallet_cubit.dart';
import 'package:waraqukum/screens/widgets/button_widget.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class TheWalletWidget extends StatelessWidget {
  final WalletCubit cubit;

  const TheWalletWidget({
    super.key,
    required this.cubit,
  });

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      const Padding(
        padding: EdgeInsets.only(bottom: 24),
        child: TextWidget(
          title:
              "هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة",
          maxLines: null,
          fontSize: 12,
          color: quartzColor,
        ),
      ),
      const Padding(
        padding: EdgeInsets.only(bottom: 48),
        child: TextWidget(
          title: "كل 100 نقطة = 200 ريال",
          fontSize: 18,
          color: secondaryColor,
          fontWeight: FontWeight.w500,
        ),
      ),
      Padding(
        padding: const EdgeInsets.only(bottom: 14),
        child: Image.asset(
          "assets/images/medal.png",
          width: 105,
          height: 125,
        ),
      ),
      const Padding(
        padding: EdgeInsets.only(bottom: 12),
        child: TextWidget(
          title: "1,421",
          fontSize: 40,
          color: secondaryColor,
          fontWeight: FontWeight.bold,
        ),
      ),
      Padding(
        padding: const EdgeInsets.only(bottom: 48),
        child: TextWidget(
          title: "ريال سعودي",
          fontSize: 14,
          fontWeight: FontWeight.bold,
          color: quartzColor.withOpacity(0.5),
        ),
      ),
      ButtonWidget(title: "تحويل رصيد", onTap: () {})
    ]);
  }
}
