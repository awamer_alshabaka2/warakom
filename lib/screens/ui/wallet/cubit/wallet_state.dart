part of 'wallet_cubit.dart';

abstract class WalletState {}

final class WalletInitial extends WalletState {}

final class ChangeWalletState extends WalletState {}
