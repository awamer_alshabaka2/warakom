import 'package:flutter_bloc/flutter_bloc.dart';

part 'wallet_state.dart';

class WalletCubit extends Cubit<WalletState> {
  WalletCubit() : super(WalletInitial());
  static WalletCubit get(context) => BlocProvider.of(context);
  bool isWallet = true;
  changeWallet({required bool isWallet}) {
    this.isWallet = isWallet;
    emit(ChangeWalletState());
  }
}
