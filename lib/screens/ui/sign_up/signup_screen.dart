import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:waraqukum/core/utils/cache_helper.dart';
import 'package:waraqukum/core/utils/statics.dart';
import 'package:waraqukum/core/utils/utils.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/core/utils/validation.dart';
import 'package:waraqukum/domain/models/user_model.dart';
import 'package:waraqukum/screens/ui/otp/otp.dart';
import 'package:waraqukum/screens/ui/sign_up/cubit/sign_up_cubit.dart';
import 'package:waraqukum/screens/widgets/button_widget.dart';
import 'package:waraqukum/screens/widgets/custom_app_bar.dart';
import 'package:waraqukum/screens/widgets/text_field_widget.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class SignUpScreen extends StatelessWidget {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  SignUpScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        const CustomAppBar(title: "إنشاء حساب"),
        Expanded(
          child: BlocProvider(
            create: (context) => SignUpCubit(),
            child: BlocConsumer<SignUpCubit, SignUpState>(
              listener: (context, state) {},
              builder: (context, state) {
                SignUpCubit cubit = SignUpCubit.get(context);
                return SingleChildScrollView(
                  child: Form(
                    key: formKey,
                    child: Column(children: [
                      const SizedBox(height: 36),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 30),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            GestureDetector(
                              onTap: () async {
                                final source =
                                    await Utils.showImageSource(context);
                                if (source == null) return;
                                cubit.pickImage(source);
                              },
                              child: Align(
                                alignment: Alignment.center,
                                child: Stack(
                                    alignment: AlignmentDirectional.bottomStart,
                                    children: [
                                      CircleAvatar(
                                        radius: 50,
                                        child: Container(
                                          clipBehavior: Clip.antiAlias,
                                          decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                          ),
                                          child: cubit.profileImage != null
                                              ? Image.file(
                                                  cubit.profileImage!,
                                                  width: 100,
                                                  height: 100,
                                                  fit: BoxFit.cover,
                                                )
                                              : Image.asset(
                                                  'assets/images/picture.png',
                                                  width: 100,
                                                  height: 100,
                                                  fit: BoxFit.cover,
                                                  errorBuilder: (context, error,
                                                          stackTrace) =>
                                                      Image.asset(
                                                    'assets/images/logo.png',
                                                    width: 100,
                                                    height: 100,
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                        ),
                                      ),
                                      Container(
                                          padding: const EdgeInsets.all(6),
                                          height: 30,
                                          width: 30,
                                          decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: secondaryColor,
                                          ),
                                          child: Image.asset(
                                            'assets/images/cam.png',
                                            width: 12,
                                            height: 10,
                                            fit: BoxFit.fill,
                                          ))
                                    ]),
                              ),
                            ),
                            const SizedBox(height: 22),
                            const TextWidget(
                              title: "الأسم",
                              fontSize: 14,
                              color: primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                            const SizedBox(height: 14),
                            TextFieldWidget(
                              controller: cubit.nameController,
                              label: "أدخل الأسم",
                              validator: Validation().defaultValidation,
                            ),
                            const SizedBox(height: 14),
                            const TextWidget(
                              title: "رقم الجوال",
                              fontSize: 14,
                              color: primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                            const SizedBox(height: 14),
                            TextFieldWidget(
                              controller: cubit.phoneController,
                              label: "أدخل رقم الجوال",
                              validator: Validation().defaultValidation,
                              suffixIcon: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Image.asset(
                                      "assets/images/saudi.png",
                                      height: 16,
                                      width: 24,
                                      fit: BoxFit.fill,
                                    ),
                                    const SizedBox(width: 4),
                                    const TextWidget(
                                      title: "+966",
                                    ),
                                    const SizedBox(width: 4),
                                    const Icon(
                                      Icons.arrow_drop_down_outlined,
                                    )
                                  ]),
                            ),
                            const SizedBox(height: 14),
                            const TextWidget(
                              title: "البريد الإلكتروني",
                              fontSize: 14,
                              color: primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                            const SizedBox(height: 14),
                            TextFieldWidget(
                              controller: cubit.emailController,
                              label: "أدخل البريد الإلكتروني",
                              validator: Validation().emailValidation,
                            ),
                            const SizedBox(height: 14),
                            const TextWidget(
                              title: "المرحلة الدراسية",
                              fontSize: 14,
                              color: primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                            const SizedBox(height: 14),
                            TextFieldWidget(
                              controller: cubit.levelController,
                              label: "أدخل المرحلة الدراسية",
                              validator: Validation().defaultValidation,
                            ),
                            const SizedBox(height: 14),
                            const TextWidget(
                              title: "كلمة المرور",
                              fontSize: 14,
                              color: primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                            const SizedBox(height: 10),
                            TextFieldWidget(
                              controller: cubit.passwordController,
                              label: "ادخل كلمة المرور",
                              validator: Validation().validatePassword,
                              password: cubit.isPassword,
                              suffixIcon: GestureDetector(
                                  onTap: () => cubit.changePasswordVisibility(
                                      isPasswordConfirm: false),
                                  child: Icon(
                                    cubit.isPassword
                                        ? Icons.visibility_outlined
                                        : Icons.visibility_off_outlined,
                                    color: const Color(0xffBCBCBC),
                                    size: 22,
                                  )),
                            ),
                            const SizedBox(height: 14),
                            const TextWidget(
                              title: "تأكيد كلمة المرور",
                              fontSize: 14,
                              color: primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                            const SizedBox(height: 10),
                            TextFieldWidget(
                              controller: cubit.confirmPasswordController,
                              label: "تأكيد كلمة المرور",
                              validator: (value) {
                                return Validation().confirmPasswordValidation(
                                    value, cubit.passwordController.text);
                              },
                              password: cubit.isPasswordConfirm,
                              suffixIcon: GestureDetector(
                                  onTap: () => cubit.changePasswordVisibility(
                                      isPasswordConfirm: true),
                                  child: Icon(
                                    cubit.isPasswordConfirm
                                        ? Icons.visibility_outlined
                                        : Icons.visibility_off_outlined,
                                    color: const Color(0xffBCBCBC),
                                    size: 22,
                                  )),
                            ),
                            const SizedBox(height: 32),
                            ButtonWidget(
                                title: "تسجيل جديد",
                                onTap: () {
                                  if (formKey.currentState!.validate()) {
                                    UserModel userModel = UserModel(
                                      photo: cubit.profileImage?.path,
                                      name: cubit.nameController.text,
                                      email: cubit.emailController.text,
                                      phone: cubit.phoneController.text,
                                      level: cubit.levelController.text,
                                      password: cubit.passwordController.text,
                                    );
                                    CacheHelper.saveData(
                                            key: Statics.userModel,
                                            value:
                                                jsonEncode(userModel.toJson()))
                                        .then((value) {
                                      Utils.openScreen(context, const Otp());
                                    });
                                  }
                                }),
                            const SizedBox(height: 32),
                          ],
                        ),
                      )
                    ]),
                  ),
                );
              },
            ),
          ),
        )
      ],
    ));
  }
}
