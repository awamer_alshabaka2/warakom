import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:waraqukum/core/utils/utils.dart';

part 'sign_up_state.dart';

class SignUpCubit extends Cubit<SignUpState> {
  SignUpCubit() : super(SignUpInitial());
  static SignUpCubit get(context) => BlocProvider.of(context);
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController emailController = TextEditingController();

  TextEditingController levelController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  bool isPassword = true;
  bool isPasswordConfirm = true;

  void changePasswordVisibility({required bool isPasswordConfirm}) {
    if (isPasswordConfirm) {
      this.isPasswordConfirm = !this.isPasswordConfirm;
    } else {
      isPassword = !isPassword;
    }
    emit(ChangePasswordVisibilityState());
  }

  File? profileImage;

  Future pickImage(ImageSource source) async {
    try {
      profileImage = await Utils.pickImage(source);
      emit(UpdateImage());
    } catch (e) {
      debugPrint('Failed to pick image: $e');
    }
  }
}
