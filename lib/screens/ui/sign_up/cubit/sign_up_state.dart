part of 'sign_up_cubit.dart';

abstract class SignUpState {}

final class SignUpInitial extends SignUpState {}

final class ChangePasswordVisibilityState extends SignUpState {}

final class UpdateImage extends SignUpState {}
