import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:waraqukum/screens/ui/home/cubit/home_cubit.dart';
import 'package:waraqukum/screens/ui/home/widgets/custom_nav_bar.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => HomeCubit(),
        child: BlocConsumer<HomeCubit, HomeState>(
          listener: (context, state) {},
          builder: (context, state) {
            HomeCubit cubit = HomeCubit.get(context);
            return Column(children: [
              Expanded(
                child: cubit.screens[cubit.currentIndex],
              ),
              CustomBottomNavBar(cubit: cubit),
            ]);
          },
        ),
      ),
    );
  }
}
