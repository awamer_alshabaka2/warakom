import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:waraqukum/core/utils/cache_helper.dart';
import 'package:waraqukum/core/utils/statics.dart';
import 'package:waraqukum/core/utils/utils.dart';
import 'package:waraqukum/domain/models/user_model.dart';
import 'package:waraqukum/screens/ui/home/screens/cart.dart';
import 'package:waraqukum/screens/ui/home/screens/main_screen/main_screen.dart';
import 'package:waraqukum/screens/ui/home/screens/more.dart';
import 'package:waraqukum/screens/ui/home/screens/purchese_screen.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit() : super(HomeInitial());
  static HomeCubit get(context) => BlocProvider.of(context);
  List<Widget> screens = [
    MainScreen(),
    const CartScreen(),
    const PurcheseScreen(),
    const MoreScreen()
  ];
  int currentIndex = 0;
  changeIndex(int index) {
    currentIndex = index;
    emit(ChangeCurrentIndex());
  }

  File? profileImage;

  Future pickImage(ImageSource source) async {
    try {
      profileImage = await Utils.pickImage(source);
      UserModel? userModel = Utils.getUserModel();
      profileImage != null ? userModel?.photo = profileImage?.path : null;
      CacheHelper.saveData(
          key: Statics.userModel, value: jsonEncode(userModel?.toJson()));
      profileImage = null;
      emit(UpdateImage());
    } catch (e) {
      debugPrint('Failed to pick image: $e');
    }
  }
}
