// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/ui/home/cubit/home_cubit.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class CustomBottomNavBar extends StatelessWidget {
  final HomeCubit cubit;
  const CustomBottomNavBar({
    super.key,
    required this.cubit,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.symmetric(vertical: 16),
        decoration: const BoxDecoration(
          color: secondaryColor,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: Row(
          children: [
            ItemOfNavBar(
              img: "assets/images/home.png",
              title: "الرئيسية",
              onTap: () => cubit.changeIndex(0),
              isSelected: cubit.currentIndex == 0,
            ),
            ItemOfNavBar(
              img: "assets/images/cart.png",
              title: "السلة",
              onTap: () => cubit.changeIndex(1),
              isSelected: cubit.currentIndex == 1,
            ),
            ItemOfNavBar(
              img: "assets/images/purchases.png",
              title: "مشترياتي",
              onTap: () => cubit.changeIndex(2),
              isSelected: cubit.currentIndex == 2,
            ),
            ItemOfNavBar(
              img: "assets/images/more.png",
              title: "المزيد",
              onTap: () => cubit.changeIndex(3),
              isSelected: cubit.currentIndex == 3,
            ),
          ],
        ));
  }
}

class ItemOfNavBar extends StatelessWidget {
  final String img;
  final String title;
  final VoidCallback onTap;
  final bool isSelected;
  const ItemOfNavBar({
    Key? key,
    required this.img,
    required this.title,
    required this.onTap,
    required this.isSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GestureDetector(
        onTap: onTap,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              img,
              width: 24,
              height: 24,
              fit: BoxFit.fill,
            ),
            const SizedBox(height: 4),
            TextWidget(
              title: title,
              color: Colors.white,
              fontWeight: FontWeight.w500,
            ),
            const SizedBox(height: 6),
            isSelected
                ? Container(
                    width: 8,
                    height: 8,
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.circle,
                    ))
                : const SizedBox()
          ],
        ),
      ),
    );
  }
}
