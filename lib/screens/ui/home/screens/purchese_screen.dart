import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/widgets/button_widget.dart';
import 'package:waraqukum/screens/widgets/custom_app_bar.dart';
import 'package:waraqukum/screens/widgets/resource_item_photo.dart';
import 'package:waraqukum/screens/widgets/resource_section2.dart';
import 'package:waraqukum/screens/widgets/resource_widget.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class PurcheseScreen extends StatelessWidget {
  const PurcheseScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const CustomAppBar(
          title: "المشتريات",
          isArrowBackIcon: false,
        ),
        Expanded(
          child: Column(children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: ListView.separated(
                  shrinkWrap: true,
                  itemBuilder: (context, index) => Column(
                    children: [
                      Row(
                        children: [
                          TeacherNameWidget(
                            image:
                                "https://images.pexels.com/photos/2599488/pexels-photo-2599488.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
                            index: index.toString(),
                          ),
                          const SizedBox(width: 26),
                          Image.asset(
                            "assets/images/unfollow.png",
                            width: 16,
                            height: 16,
                            fit: BoxFit.fill,
                          ),
                          const SizedBox(width: 8),
                          const TextWidget(title: "الغاء متابعة")
                        ],
                      ),
                      const Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          SizedBox(
                            width: 114,
                            child: ResourceItemPhoto(
                              image:
                                  "https://images.pexels.com/photos/2599488/pexels-photo-2599488.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
                              isFavouriteIcon: false,
                              height: 100,
                            ),
                          ),
                          SizedBox(width: 18),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ResourceSection2(isCartScreen: false),
                              ],
                            ),
                          )
                        ],
                      ),
                      const SizedBox(height: 8),
                      const Row(children: [
                        Expanded(
                          child: ButtonWidget(
                            title: "تحميل",
                            borderRadius: BorderRadiusDirectional.only(
                              bottomStart: Radius.circular(8),
                            ),
                          ),
                        ),
                        SizedBox(width: 2),
                        Expanded(
                          child: ButtonWidget(
                            title: "تقييم",
                            buttonColor: blue,
                            borderRadius: BorderRadiusDirectional.only(
                              bottomStart: Radius.circular(8),
                            ),
                          ),
                        ),
                      ])
                    ],
                  ),
                  separatorBuilder: (context, index) =>
                      const SizedBox(height: 16),
                  itemCount: 3,
                ),
              ),
            )
          ]),
        ),
      ],
    );
  }
}
