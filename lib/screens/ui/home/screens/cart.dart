import 'package:flutter/material.dart';
import 'package:waraqukum/screens/ui/home/screens/main_screen/widgets/last_section.dart';
import 'package:waraqukum/screens/widgets/custom_app_bar.dart';
import 'package:waraqukum/screens/widgets/resource_item_photo.dart';
import 'package:waraqukum/screens/widgets/resource_section2.dart';

class CartScreen extends StatelessWidget {
  const CartScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const CustomAppBar(
          title: "السلة",
          isArrowBackIcon: false,
        ),
        Expanded(
          child: Column(children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: ListView.separated(
                  shrinkWrap: true,
                  itemBuilder: (context, index) => const Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SizedBox(
                          width: 114,
                          child: ResourceItemPhoto(
                            image:
                                "https://images.pexels.com/photos/2599488/pexels-photo-2599488.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
                            isFavouriteIcon: false,
                            height: 120,
                          ),
                        ),
                        SizedBox(width: 18),
                        Expanded(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ResourceSection2(
                              isCartScreen: true,
                              isFree: true,
                            ),
                          ],
                        ))
                      ]),
                  separatorBuilder: (context, index) =>
                      const SizedBox(height: 16),
                  itemCount: 10,
                ),
              ),
            )
          ]),
        ),
        const LastSection(),
      ],
    );
  }
}
