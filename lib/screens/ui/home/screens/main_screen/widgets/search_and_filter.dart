import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/core/utils/validation.dart';
import 'package:waraqukum/screens/widgets/text_field_widget.dart';

class SearchAndFilter extends StatelessWidget {
  const SearchAndFilter({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 8),
      child: Row(children: [
        Container(
          alignment: Alignment.center,
          width: 40,
          height: 40,
          decoration: BoxDecoration(
              color: secondaryColor, borderRadius: BorderRadius.circular(5)),
          child: Image.asset(
            "assets/images/filter.png",
            width: 20,
            height: 20,
            fit: BoxFit.fill,
          ),
        ),
        const SizedBox(width: 32),
        Expanded(
          child: TextFieldWidget(
            label: "ابحث عن المنتجات والفئات",
            validator: Validation().defaultValidation,
          ),
        ),
        Container(
          alignment: Alignment.center,
          width: 40,
          height: 40,
          decoration: BoxDecoration(
              color: secondaryColor, borderRadius: BorderRadius.circular(5)),
          child: Image.asset(
            "assets/images/search.png",
            width: 20,
            height: 20,
            fit: BoxFit.fill,
          ),
        ),
      ]),
    );
  }
}
