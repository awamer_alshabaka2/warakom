import 'package:flutter/material.dart';
import 'package:waraqukum/screens/widgets/button_widget.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class LastSection extends StatelessWidget {
  const LastSection({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Padding(
          padding: EdgeInsets.symmetric(vertical: 16),
          child: TextWidget(
            title:
                "يمكنك تحميل المنتجات التي تم شراؤها على الفور من حساب ورقكم الخاص بك",
            textAlign: TextAlign.center,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 16),
          child: ButtonWidget(
            title: "إتمام الدفع",
            onTap: () {},
          ),
        ),
      ],
    );
  }
}
