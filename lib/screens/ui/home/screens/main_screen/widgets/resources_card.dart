import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/core/utils/utils.dart';
import 'package:waraqukum/screens/ui/product_details/product_details.dart';
import 'package:waraqukum/screens/widgets/resource_widget.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class ResourcesCard extends StatelessWidget {
  final String spacialHeroName;
  const ResourcesCard({
    super.key,
    required this.imagesList,
    required this.spacialHeroName,
  });

  final List<String> imagesList;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsetsDirectional.only(start: 18, top: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextWidget(
            title: "استكشف موارد المدرسة الثانوية",
            color: Colors.black.withOpacity(0.60),
            fontWeight: FontWeight.bold,
          ),
          const SizedBox(height: 14),
          Column(
            children: [
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    for (int i = 0; i < 10; i++)
                      SizedBox(
                        width: 160,
                        child: Padding(
                          padding: const EdgeInsetsDirectional.only(end: 8),
                          child: GestureDetector(
                            onTap: () {
                              Utils.openScreen(
                                  context,
                                  ProductDetails(
                                    index: spacialHeroName + i.toString(),
                                  ));
                            },
                            child: ResourceWidget(
                              index: spacialHeroName + i.toString(),
                              image: imagesList.first,
                              height: 104,
                              isHomeScren: true,
                            ),
                          ),
                        ),
                      ),
                  ],
                ),
              ),
              const SizedBox(height: 18),
              const Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                TextWidget(
                  title: "مشاهدة المزبد",
                  fontWeight: FontWeight.bold,
                  color: secondaryColor,
                ),
                SizedBox(height: 8),
                Icon(
                  Icons.arrow_forward_ios,
                  color: secondaryColor,
                )
              ])
            ],
          )
        ],
      ),
    );
  }
}
