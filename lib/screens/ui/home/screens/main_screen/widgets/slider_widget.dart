import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:waraqukum/screens/widgets/shimmer_loading_skeleton.dart';

class SliderWidget extends StatelessWidget {
  const SliderWidget({
    super.key,
    required this.imagesList,
  });

  final List<String> imagesList;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 18, right: 18, bottom: 10),
      child: CarouselSlider(
        items: imagesList
            .map(
              (e) => Container(
                clipBehavior: Clip.antiAliasWithSaveLayer,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(9),
                ),
                child: CachedNetworkImage(
                  imageUrl: e,
                  width: double.infinity,
                  fit: BoxFit.cover,
                  placeholder: (context, url) => const ShimmerLoading(
                    height: double.infinity,
                    shape: BoxShape.rectangle,
                  ),
                  errorWidget: (context, url, error) => const ShimmerLoading(
                    width: double.infinity,
                    height: double.infinity,
                    shape: BoxShape.rectangle,
                  ),
                ),
              ),
            )
            .toList(),
        options: CarouselOptions(
          height: 136,
          viewportFraction: 1,
          enlargeCenterPage: false,
          clipBehavior: Clip.antiAliasWithSaveLayer,
          initialPage: 0,
          enableInfiniteScroll: true,
          autoPlay: true,
          autoPlayInterval: const Duration(seconds: 3),
          autoPlayAnimationDuration: const Duration(seconds: 1),
          autoPlayCurve: Curves.fastOutSlowIn,
          scrollDirection: Axis.horizontal,
        ),
      ),
    );
  }
}
