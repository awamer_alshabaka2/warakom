import 'package:flutter/material.dart';
import 'package:waraqukum/core/utils/utils.dart';
import 'package:waraqukum/screens/ui/home/screens/main_screen/widgets/resources_card.dart';
import 'package:waraqukum/screens/ui/home/screens/main_screen/widgets/search_and_filter.dart';
import 'package:waraqukum/screens/ui/home/screens/main_screen/widgets/slider_widget.dart';
import 'package:waraqukum/screens/ui/notification_screen.dart/notification_screen.dart';
import 'package:waraqukum/screens/widgets/custom_app_bar.dart';

class MainScreen extends StatelessWidget {
  final List<String> imagesList = [
    "https://images.pexels.com/photos/2599488/pexels-photo-2599488.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
    "https://images.pexels.com/photos/19240445/pexels-photo-19240445/free-photo-of-a-man-in-a-white-sweater-is-sitting-at-a-desk-with-a-laptop.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
    "https://images.pexels.com/photos/9493792/pexels-photo-9493792.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
  ];
  MainScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        CustomAppBar(
          title: "الرئيسية",
          isArrowBackIcon: false,
          padding:
              const EdgeInsets.only(top: 40, right: 18, left: 18, bottom: 20),
          height: 140,
          actionWidget: GestureDetector(
            onTap: () => Utils.openScreen(context, const NotificationsScreen()),
            child: Container(
              alignment: Alignment.center,
              child: SizedBox(
                child: Image.asset(
                  "assets/images/noti.png",
                  width: 24,
                  height: 24,
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 120),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SliderWidget(imagesList: imagesList),
              const SearchAndFilter(),
              Expanded(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16),
                    child: Column(
                      children: [
                        ResourcesCard(
                            imagesList: imagesList, spacialHeroName: "Hero1"),
                        ResourcesCard(
                            imagesList: imagesList, spacialHeroName: "Hero2"),
                        ResourcesCard(
                            imagesList: imagesList, spacialHeroName: "Hero3"),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ]),
    );
  }
}
