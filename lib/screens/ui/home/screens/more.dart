// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:waraqukum/core/utils/utils.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/screens/ui/complaints/complaints.dart';
import 'package:waraqukum/screens/ui/contact_us/contact_us.dart';
import 'package:waraqukum/screens/ui/payment_cards/payment_cards_screen.dart';
import 'package:waraqukum/screens/ui/favourite/favourite.dart';
import 'package:waraqukum/screens/ui/home/cubit/home_cubit.dart';
import 'package:waraqukum/screens/ui/invoices/invoices.dart';
import 'package:waraqukum/screens/ui/profile/profile.dart';
import 'package:waraqukum/screens/ui/service_providers/service_providers.dart';
import 'package:waraqukum/screens/ui/settings/settings.dart';
import 'package:waraqukum/screens/ui/wallet/wallet.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class MoreScreen extends StatefulWidget {
  const MoreScreen({super.key});

  @override
  State<MoreScreen> createState() => _MoreScreenState();
}

class _MoreScreenState extends State<MoreScreen> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        Container(
          height: 120,
          decoration: const BoxDecoration(
            color: secondaryColor,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30),
            ),
          ),
          child: const Padding(
            padding: EdgeInsets.only(bottom: 24),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 12),
              child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    children: [
                      Expanded(child: SizedBox()),
                      Expanded(
                        child: TextWidget(
                            title: "المزيد",
                            color: Colors.white,
                            fontSize: 16,
                            textAlign: TextAlign.center,
                            fontWeight: FontWeight.w500),
                      ),
                      Expanded(
                        child: SizedBox(),
                      )
                    ],
                  )),
            ),
          ),
        ),
        Expanded(
          child: BlocProvider(
            create: (context) => HomeCubit(),
            child: BlocConsumer<HomeCubit, HomeState>(
              listener: (context, state) {},
              builder: (context, state) {
                HomeCubit cubit = HomeCubit.get(context);
                return SingleChildScrollView(
                  child: Column(children: [
                    const SizedBox(height: 36),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          GestureDetector(
                            onTap: () async {
                              final source =
                                  await Utils.showImageSource(context);
                              if (source == null) return;
                              cubit.pickImage(source);
                            },
                            child: Align(
                              alignment: Alignment.center,
                              child: Stack(
                                  alignment: AlignmentDirectional.bottomStart,
                                  children: [
                                    CircleAvatar(
                                      radius: 50,
                                      child: Container(
                                          clipBehavior: Clip.antiAlias,
                                          decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                          ),
                                          child: cubit.profileImage == null &&
                                                  Utils.getUserModel()?.photo ==
                                                      null
                                              ? Image.asset(
                                                  'assets/images/logo.png',
                                                  width: 100,
                                                  height: 100,
                                                  fit: BoxFit.cover,
                                                )
                                              : Image.file(
                                                  cubit.profileImage ??
                                                      File(Utils.getUserModel()
                                                              ?.photo ??
                                                          ''),
                                                  width: 100,
                                                  height: 100,
                                                  fit: BoxFit.cover,
                                                )),
                                    ),
                                    Container(
                                        padding: const EdgeInsets.all(6),
                                        height: 30,
                                        width: 30,
                                        decoration: const BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: secondaryColor,
                                        ),
                                        child: Image.asset(
                                          'assets/images/cam.png',
                                          width: 12,
                                          height: 10,
                                          fit: BoxFit.fill,
                                        ))
                                  ]),
                            ),
                          ),
                          const SizedBox(height: 22),
                          ItemOfMore(
                            onTap: () async =>
                                Utils.openScreen(context, ProfileScreen())
                                    ?.then((_) {
                              setState(() {});
                            }),
                            img: 'assets/images/user.png',
                            title: 'حسابي',
                          ),
                          ItemOfMore(
                            onTap: () {
                              Utils.openScreen(context, const WalletScreen());
                            },
                            img: 'assets/images/poket.png',
                            title: 'المحفظة',
                          ),
                          ItemOfMore(
                            onTap: () {
                              Utils.openScreen(context, const InvoicesScreen());
                            },
                            img: 'assets/images/purchese.png',
                            title: 'الفواتير',
                          ),
                          ItemOfMore(
                            onTap: () {
                              Utils.openScreen(
                                  context, const PaymentCardsScreen());
                            },
                            img: 'assets/images/cards.png',
                            title: 'بطاقات الدفع',
                          ),
                          ItemOfMore(
                            onTap: () {
                              Utils.openScreen(
                                  context, const ServiceProviders());
                            },
                            img: 'assets/images/person.png',
                            title: 'مقدمي الخدمة المتابعيين',
                          ),
                          ItemOfMore(
                            onTap: () {},
                            img: 'assets/images/Group.png',
                            title: 'كن مقدم خدمة',
                          ),
                          ItemOfMore(
                            onTap: () {
                              Utils.openScreen(
                                context,
                                const FavouriteScren(),
                              );
                            },
                            img: 'assets/images/hearts.png',
                            title: 'المفضلة',
                          ),
                          ItemOfMore(
                            onTap: () {},
                            img: 'assets/images/star1.png',
                            title: 'قيمنا',
                          ),
                          ItemOfMore(
                            onTap: () {
                              Utils.openScreen(
                                  context, const ContactUsScreen());
                            },
                            img: 'assets/images/call.png',
                            title: 'تواصل معنا',
                          ),
                          ItemOfMore(
                            onTap: () {
                              Utils.openScreen(
                                  context, const ComplaintsScreen());
                            },
                            img: 'assets/images/feather-share.png',
                            title: 'الشكاوي والمقترحات',
                          ),
                          ItemOfMore(
                            onTap: () {},
                            img: 'assets/images/share.png',
                            title: 'مشاركة المنصة',
                          ),
                          ItemOfMore(
                            onTap: () {
                              Utils.openScreen(context, SettingsScreen());
                            },
                            img: 'assets/images/setting.png',
                            title: 'الإعدادات',
                          ),
                          const SizedBox(height: 32),
                        ],
                      ),
                    )
                  ]),
                );
              },
            ),
          ),
        )
      ],
    ));
  }
}

class ItemOfMore extends StatelessWidget {
  final VoidCallback onTap;
  final String img;
  final String title;
  const ItemOfMore({
    Key? key,
    required this.onTap,
    required this.img,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: ListTile(
        leading: Padding(
          padding: const EdgeInsets.only(bottom: 8),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                padding: const EdgeInsets.all(8),
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color(0xff81ACAE),
                ),
                child: Image.asset(
                  img,
                  width: 16,
                  height: 16,
                  fit: BoxFit.fill,
                ),
              ),
              const SizedBox(width: 8),
              TextWidget(
                title: title,
                fontSize: 12,
                fontWeight: FontWeight.bold,
              ),
            ],
          ),
        ),
        trailing: const Icon(
          Icons.arrow_back_ios_new,
          color: secondaryColor,
          size: 18,
        ),
      ),
    );
  }
}
