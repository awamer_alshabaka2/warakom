import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:waraqukum/core/utils/cache_helper.dart';
import 'package:waraqukum/core/utils/statics.dart';
import 'package:waraqukum/core/utils/utils.dart';
import 'package:waraqukum/core/utils/colors.dart';
import 'package:waraqukum/core/utils/validation.dart';
import 'package:waraqukum/domain/models/user_model.dart';
import 'package:waraqukum/screens/ui/change_password/cubit/change_password_cubit.dart';
import 'package:waraqukum/screens/widgets/button_widget.dart';
import 'package:waraqukum/screens/widgets/text_field_widget.dart';
import 'package:waraqukum/screens/widgets/text_widget.dart';

class ChangePassword extends StatelessWidget {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  ChangePassword({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        Container(
          height: 120,
          decoration: const BoxDecoration(
            color: secondaryColor,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.only(bottom: 24),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    children: [
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Image.asset(
                            "assets/images/arrow.png",
                            alignment: AlignmentDirectional.centerStart,
                            width: 24,
                            height: 24,
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                      const Expanded(
                        flex: 2,
                        child: TextWidget(
                            title: "تغيير كلمة المرور",
                            color: Colors.white,
                            fontSize: 16,
                            textAlign: TextAlign.center,
                            fontWeight: FontWeight.w500),
                      ),
                      const Expanded(
                        child: SizedBox(),
                      )
                    ],
                  )),
            ),
          ),
        ),
        Expanded(
          child: BlocProvider(
            create: (context) => ChangePasswordCubit(),
            child: BlocConsumer<ChangePasswordCubit, ChangePasswordState>(
              listener: (context, state) {},
              builder: (context, state) {
                ChangePasswordCubit cubit = ChangePasswordCubit.get(context);
                return SingleChildScrollView(
                  child: Form(
                    key: formKey,
                    child: Column(children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 30),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(height: 32),
                            const TextWidget(
                              title: "كلمة المرور القديمة",
                              fontSize: 14,
                              color: primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                            const SizedBox(height: 10),
                            TextFieldWidget(
                              controller: cubit.oldPasswordController,
                              label: "كلمة المرور القديمة",
                              password: cubit.isVisibilityOldPassword,
                              validator: (value) {
                                if (value != null && value.isNotEmpty) {
                                  return Validation().confirmPasswordValidation(
                                      Utils.getUserModel()?.password,
                                      cubit.oldPasswordController.text);
                                } else {
                                  return "هذا الحقل مطلوب";
                                }
                              },
                              suffixIcon: GestureDetector(
                                  onTap: () =>
                                      cubit.changeOldPasswordVisibility(),
                                  child: Icon(
                                    cubit.isVisibilityOldPassword
                                        ? Icons.visibility_outlined
                                        : Icons.visibility_off_outlined,
                                    color: const Color(0xffBCBCBC),
                                    size: 22,
                                  )),
                            ),
                            const SizedBox(height: 14),
                            const TextWidget(
                              title: "كلمة المرور الجديدة",
                              fontSize: 14,
                              color: primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                            const SizedBox(height: 10),
                            TextFieldWidget(
                              controller: cubit.newPasswordController,
                              label: "كلمة المرور الجديدة",
                              password: cubit.isNewPassword,
                              validator: Validation().validatePassword,
                              suffixIcon: GestureDetector(
                                  onTap: () => cubit.changenewPassword(),
                                  child: Icon(
                                    cubit.isNewPassword
                                        ? Icons.visibility_outlined
                                        : Icons.visibility_off_outlined,
                                    color: const Color(0xffBCBCBC),
                                    size: 22,
                                  )),
                            ),
                            const SizedBox(height: 14),
                            const TextWidget(
                              title: "تأكيد كلمة المرور الجديدة",
                              fontSize: 14,
                              color: primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                            const SizedBox(height: 10),
                            TextFieldWidget(
                              label: "تأكيد كلمة المرور الجديدة",
                              validator: (value) {
                                return Validation().confirmPasswordValidation(
                                    value, cubit.newPasswordController.text);
                              },
                              password: cubit.isNewPasswordConfirm,
                              suffixIcon: GestureDetector(
                                  onTap: () => cubit.changeConfirmPassword(),
                                  child: Icon(
                                    cubit.isNewPasswordConfirm
                                        ? Icons.visibility_outlined
                                        : Icons.visibility_off_outlined,
                                    color: const Color(0xffBCBCBC),
                                    size: 22,
                                  )),
                            ),
                            const SizedBox(height: 46),
                            Align(
                              alignment: AlignmentDirectional.center,
                              child: ButtonWidget(
                                  title: "حفظ",
                                  onTap: () {
                                    if (formKey.currentState!.validate()) {
                                      UserModel? userModel =
                                          Utils.getUserModel();
                                      userModel?.password =
                                          cubit.newPasswordController.text;
                                      CacheHelper.saveData(
                                          key: Statics.userModel,
                                          value:
                                              jsonEncode(userModel?.toJson()));
                                      Navigator.pop(context);
                                    }
                                  }),
                            ),
                          ],
                        ),
                      )
                    ]),
                  ),
                );
              },
            ),
          ),
        )
      ],
    ));
  }
}
