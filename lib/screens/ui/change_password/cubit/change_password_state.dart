part of 'change_password_cubit.dart';

abstract class ChangePasswordState {}

final class ChangePasswordInitial extends ChangePasswordState {}

final class ChangeOldPasswordVisibility extends ChangePasswordState {}

final class ChangeNewPasswordVisibilityState extends ChangePasswordState {}

final class ChangeConfirmPasswordVisibilityState extends ChangePasswordState {}
