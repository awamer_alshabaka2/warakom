import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
part 'change_password_state.dart';

class ChangePasswordCubit extends Cubit<ChangePasswordState> {
  ChangePasswordCubit() : super(ChangePasswordInitial());
  static ChangePasswordCubit get(context) => BlocProvider.of(context);
  TextEditingController oldPasswordController = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  bool isVisibilityOldPassword = true;
  bool isNewPassword = true;
  bool isNewPasswordConfirm = true;

  void changeOldPasswordVisibility() {
    isVisibilityOldPassword = !isVisibilityOldPassword;
    emit(ChangeOldPasswordVisibility());
  }

  void changenewPassword() {
    isNewPassword = !isNewPassword;
    emit(ChangeNewPasswordVisibilityState());
  }

  void changeConfirmPassword() {
    isNewPasswordConfirm = !isNewPasswordConfirm;
    emit(ChangeConfirmPasswordVisibilityState());
  }
}
