import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:waraqukum/core/utils/cache_helper.dart';
import 'package:waraqukum/core/utils/responsive_framework.dart';
import 'package:waraqukum/screens/ui/splash/splash_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  await CacheHelper.init();
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarBrightness: Brightness
        .light, // this will change the brightness of the icons for ios
    statusBarIconBrightness: Brightness
        .dark, // this will change the brightness of the icons for Android
    statusBarColor: Colors.transparent, // or any color you want
  ));
  runApp(EasyLocalization(
      supportedLocales: const [
        Locale('ar', 'EG'),
        Locale('en', 'US'),
      ],
      startLocale: const Locale('ar', 'EG'),
      saveLocale: true,
      fallbackLocale: const Locale('ar', 'EG'),
      path: 'assets/translations',
      child: const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: (_, child) {
        return AppResponsiveWrapper(
          child: child,
        );
      },
      title: 'Waraqukum',
      debugShowCheckedModeBanner: false,
      locale: context.locale,
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
        fontFamily: 'URW',
      ),
      home: const SplashScreen(),
    );
  }
}
