class UserModel {
  String? photo;
  String? name;
  String? email;
  String? phone;
  String? level;
  String? password;

  UserModel({
    this.photo,
    this.name,
    this.email,
    this.phone,
    this.level,
    this.password,
  });

  UserModel.fromJson(Map<String, dynamic> json) {
    photo = json['photo'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    level = json['level'];
    password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['photo'] = photo;
    data['name'] = name;
    data['email'] = email;
    data['phone'] = phone;
    data['level'] = level;
    data['password'] = password;
    return data;
  }
}
